declare module "react-native-dotenv" {
  export const API_ENDPOINT: string;
  export const LOG_LEVEL: "trace" | "debug" | "info" | "warn" | "error";
}

declare module "react-native-video" {
  const Video: any;
  export default Video;
}
