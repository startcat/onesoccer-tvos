module.exports = {
  presets: [
    "module:metro-react-native-babel-preset",
    "module:react-native-dotenv"
  ],
  plugins: [
    [
      "module-resolver",
      {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
        alias: {
          "^@internal/(.+)": ([, name]) => {
            return `${__dirname}/src/${name}`;
          }
        }
      }
    ]
  ]
};
