## project setup

### system dependencies

first, we need to have installed `react-ntive-cli`, `watchman` and `carthage` installed:

```bash
brew install watchman carthage
npm install -g react-native-cli
```

### install `npm` dependencies

in the project root:

```bash
npm i
```

### build ios / tvos third parties dependencies

```bash
cd ios/
carthage update --platform tvos
```

### development flow

- open a terminal and run `./start [--reset-cache]` in the project's root directory.
- now open `xcode`, select the `OneSoccer-tvOS` target and run.

**IMPORTANT:** if the first time you run the app `xcode` complaints, just run a second time.

### publish

- just archive the `OneSoccer-tvOS` for `Generic tvOS Device``
- if a macos terminal window pop up and blames about something, just ignore it.

### that are those shell script in the root directory?

- `react-native-xcode.sh`, slightly modified RN script that xcode will trigger silently in order to bundle js source code. there's no need to touch it.
- `start` just starts the metro bundler. we can pass the option `--reset-cache` in order to tell metro to reset the cache.
- `clean_start` cleans everything (RN tmp internals, caches, etc.) and starts the bundler with the `--reset-cache`. use in extremely weird situations.
- `bundle` bundles all the sources in a js bundle. you can ignore it, since xcode will trigger it for you, but keep the file if we need to check the bundle in the future.
