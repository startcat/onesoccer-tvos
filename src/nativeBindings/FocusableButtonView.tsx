import { requireNativeComponent } from "react-native";
import { ViewComponent } from "@internal/components/types";

export type FocusableButtonViewProps = {
  focusedColor: string;
  onPress?: () => void;
};

export const FocusableButtonView: ViewComponent<
  FocusableButtonViewProps
> = requireNativeComponent("FocusableButtonView");
