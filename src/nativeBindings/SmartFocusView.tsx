import { requireNativeComponent } from "react-native";
import { ViewComponent } from "@internal/components/types";

export type SmartFocusEvent = (event: {
  nativeEvent: { isFocused: boolean };
}) => void;

export type InteractiveArea = "menu" | "main";

export type SmartFocusViewProps = {
  needsFocusGuide?: boolean;
  areaType?: InteractiveArea;
  onFocusDidChange?: SmartFocusEvent;
  claimsFirstFocus?: boolean;
  doNotStorePreferredSmartFocus?: boolean;
};

export const SmartFocusView: ViewComponent<
  SmartFocusViewProps
> = requireNativeComponent("SmartFocusView");
