import { requireNativeComponent, TextStyle } from "react-native";

import { ViewComponent } from "@internal/components/types";

export type TVTextFieldViewProps = {
  font: TextStyle;
  textValue: string;
  placeholderValue: string;
  secureTextEntry?: boolean;
  onChangeText: (event: { nativeEvent: { text: string } }) => void;
};

export const TVTextFieldView: ViewComponent<
  TVTextFieldViewProps
> = requireNativeComponent("TVTextFieldView");
