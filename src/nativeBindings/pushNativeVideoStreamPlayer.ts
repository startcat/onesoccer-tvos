import { NativeModules } from "react-native";

const VideoManager = NativeModules.VideoManager;

export type VideoMeta = {
  manifestUri: string;
  userEmail: string;
  contentTitle: string;
  contentIsLive: boolean;
};

export type PushNativeVideoStreamPlayerCallback = () => void;

export const pushNativeVideoStreamPlayer = (meta: VideoMeta) => {
  VideoManager.pushVideoStream(meta);
};
