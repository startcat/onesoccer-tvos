import { requireNativeComponent } from "react-native";
import { ViewComponent } from "@internal/components/types";

import Animated from "react-native-reanimated";
(Animated as any).addWhitelistedNativeProps({ animation: true });

export type BlurredBackgroundViewProps = {
  animationValue: any;
};

const BlurredBackgroundViewBase = requireNativeComponent(
  "BlurredBackgroundView"
) as ViewComponent;

const BlurredBackgroundView: ViewComponent<
  BlurredBackgroundViewProps
> = Animated.createAnimatedComponent(BlurredBackgroundViewBase);

export default BlurredBackgroundView;
