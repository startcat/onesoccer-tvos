import { NativeModules } from "react-native";

const NativeScreensManager = NativeModules.NativeScreensManager;

export const pushNativeScreen = (componentName: string) => {
  NativeScreensManager.pushScreen(componentName);
};
