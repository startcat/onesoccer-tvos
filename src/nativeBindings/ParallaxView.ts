import { requireNativeComponent } from "react-native";
import { ViewComponent } from "@internal/components/types";

export type ParallaxViewProps = {
  onPress?: () => void;
  cornerRadius?: number;
};

export const ParallaxView: ViewComponent<
  ParallaxViewProps
> = requireNativeComponent("ParallaxView");

// image view

export type ParallaxImageViewProps = ParallaxViewProps & { imageUrl: string };

export const ParallaxImageView: ViewComponent<
  ParallaxImageViewProps
> = requireNativeComponent("ParallaxImageView");
