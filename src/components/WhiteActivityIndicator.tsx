import React from "react";

import { ViewComponent } from "@internal/components/types";
import { ActivityIndicator } from "react-native";

export const WhiteActivityIndicator: ViewComponent = () => {
  return <ActivityIndicator size="large" color="white" />;
};
