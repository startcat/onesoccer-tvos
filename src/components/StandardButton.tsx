import React from "react";

import { Text, StyleSheet, ViewStyle, TextStyle } from "react-native";

import typography from "@internal/styles/typography";
import { ViewComponent } from "@internal/components/types";
import constants from "@internal/styles/constants";
import colors from "@internal/styles/colors";
import { ParallaxView } from "@internal/nativeBindings/ParallaxView";

export type StandardButtonProps = { title: string; onPress: () => void };
export const StandardButton: ViewComponent<StandardButtonProps> = props => {
  const { style } = props;
  return (
    <ParallaxView style={[style, styles.container]} onPress={props.onPress}>
      <Text style={[typography.circularBold, styles.text]}>{props.title}</Text>
    </ParallaxView>
  );
};

type Style = {
  container: ViewStyle;
  text: TextStyle;
};

const styles = StyleSheet.create<Style>({
  container: {
    ...constants.BUTTON_SIZE,
    backgroundColor: colors.NIGHT,
    borderRadius: 6,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: colors.WHITE
  }
});
