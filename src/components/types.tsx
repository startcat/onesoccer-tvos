import { ViewProps } from "react-native";

export type ViewComponent<M = {}> = React.FunctionComponent<ViewProps & M>;
