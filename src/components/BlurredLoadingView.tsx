import React, { useEffect } from "react";

import { ViewStyle, StyleSheet, View, Text } from "react-native";

import Animated, { Easing } from "react-native-reanimated";

import { ViewComponent } from "@internal/components/types";

import BlurredBackgroundView from "@internal/nativeBindings/BlurredBackgroundView";
import constants from "@internal/styles/constants";
import { WhiteActivityIndicator } from "@internal/components/WhiteActivityIndicator";

const makeAnim = (
  to: Animated.Value<number>,
  duration: number,
  onEnd: (v: number) => void
) => {
  const {
    Clock,
    Value,
    startClock,
    clockRunning,
    stopClock,
    onChange,
    cond,
    block,
    timing,
    eq,
    not,
    call,
    set
    //debug
  } = Animated;

  const clock = new Clock();

  const state = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0)
  };

  const config = {
    duration: duration,
    toValue: to,
    easing: Easing.inOut(Easing.ease)
  };

  return block([
    onChange(config.toValue, [cond(clockRunning(clock), stopClock(clock))]),
    cond(
      not(clockRunning(clock)),
      [
        // if clock is running
        set(state.finished, 0),
        set(state.time, 0),
        set(state.frameTime, 0),
        startClock(clock)
      ],
      [
        // if clock no running
        cond(eq(state.finished, 1), [
          // if finished
          stopClock(clock),
          call([state.position], args => {
            onEnd(args[0]);
          })
        ])
      ]
    ),
    timing(clock, state, config),
    //debug("state.position", state.position),
    state.position
  ]);
};

export type BlurredViewProps = {
  isVisible: boolean;
};

export const BlurredLoadingView: ViewComponent<BlurredViewProps> = props => {
  const { style, isVisible, ...rest } = props;

  const [_isVisible, _setIsVisible] = React.useState(isVisible);

  const to = isVisible ? 1 : 0;
  const [toValue] = React.useState(new Animated.Value(to));
  const [animation] = React.useState(
    makeAnim(toValue, constants.ANIMATION_DURATION, (v: number) => {
      _setIsVisible(v === 1);
    })
  );

  useEffect(() => {
    // REMEMBER: somehow this is going to fire the animation node
    toValue.setValue(to);

    if (isVisible) {
      _setIsVisible(true);
    }
  }, [isVisible]);

  return (
    (_isVisible && (
      <BlurredBackgroundView
        {...rest}
        animationValue={animation}
        style={[style, styles.container]}
      >
        <Animated.View style={{ opacity: animation }}>
          <WhiteActivityIndicator />
        </Animated.View>
      </BlurredBackgroundView>
    )) ||
    null
  );
};

interface ComponentStyle {
  container: ViewStyle;
}

const styles = StyleSheet.create<ComponentStyle>({
  container: {
    justifyContent: "center",
    alignItems: "center"
  }
});
