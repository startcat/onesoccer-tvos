import React from "react";

import { Text, StyleSheet, ViewStyle, TextStyle } from "react-native";

import typography from "@internal/styles/typography";
import { ViewComponent } from "@internal/components/types";
import constants from "@internal/styles/constants";
import colors from "@internal/styles/colors";
import { ParallaxView } from "@internal/nativeBindings/ParallaxView";

export type OutlinedButtonProps = { title: string; onPress: () => void };
export const OutlinedButton: ViewComponent<OutlinedButtonProps> = props => {
  const { style } = props;
  return (
    <ParallaxView style={[style, styles.container]} onPress={props.onPress}>
      <Text style={[typography.circularBold, styles.text]}>{props.title}</Text>
    </ParallaxView>
  );
};

type Style = {
  container: ViewStyle;
  text: TextStyle;
};

const styles = StyleSheet.create<Style>({
  container: {
    ...constants.BUTTON_SIZE,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: constants.BUTTON_BORDER_RADIUS,
    borderColor: colors.WHITE
  },
  text: {
    color: colors.WHITE
  }
});
