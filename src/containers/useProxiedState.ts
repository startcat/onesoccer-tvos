import { useState, useEffect } from "react";
import logger from "@internal/libs/logger";

export function makeProxiedState<T>(value: T): { value: T } {
  return new Proxy(
    { value: value },
    {
      set: function(obj, prop, value) {
        logger.debug("proxy setter");
        return true;
      }
    }
  );
}

export function useProxiedState<T>(obvervable: {
  value: T;
}): [T, (value: T) => void] {
  const [state, setState] = useState<T>(obvervable.value);

  const [proxy] = useState<{ value: T }>(
    new Proxy(obvervable, {
      set: function(obj, prop, value) {
        logger.debug("proxy setter");
        setState(value);
        return true;
      }
    })
  );

  return [
    state,
    (value: T): void => {
      logger.debug("custom setter");
      proxy.value = value;
    }
  ];
}
