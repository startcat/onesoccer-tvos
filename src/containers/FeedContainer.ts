import { useState } from "react";

import { createContainer } from "unstated-next";

import { DataProvider } from "recyclerlistview";

import getFeed, { FeedItem } from "@internal/network/services/getFeed";
import { authFlow, LoginState } from "@internal/containers/SessionContainer";
import { FEED_PAGE_SIZE } from "@internal/config";

export enum FeedState {
  UNKKNOWN = 0,
  FETCHING,
  FETCHING_FULL_REFRESH,
  READY,
  ERROR
}

const dataProviderWithData = (
  dp: DataProvider | null,
  data: Array<FeedItem>,
  refresh: boolean = false
): DataProvider => {
  const dataProvider =
    dp !== null
      ? dp
      : new DataProvider((r1: FeedItem, r2: FeedItem) => {
          return r1.id !== r2.id;
        });
  return dataProvider.cloneWithRows(
    refresh ? data : [...dataProvider.getAllData(), ...data]
  );
};

const useFeed = () => {
  const [feedState, setFeedState] = useState<{
    state: FeedState;
    pages: number;
    total: number;
    dataProvider: DataProvider | null;
  }>({ state: FeedState.UNKKNOWN, pages: 0, total: 0, dataProvider: null });

  const addPage = async (refresh: boolean = false) => {
    if (
      feedState.state === FeedState.FETCHING ||
      feedState.state === FeedState.FETCHING_FULL_REFRESH
    ) {
      return;
    }

    setFeedState({
      ...feedState,
      state: refresh ? FeedState.FETCHING_FULL_REFRESH : FeedState.FETCHING
    });

    const offset = feedState.total % FEED_PAGE_SIZE;
    const nextPages =
      (refresh ? 0 : feedState.pages) + (refresh || offset === 0 ? 1 : 0);
    const limit = FEED_PAGE_SIZE - (refresh ? 0 : offset);
    const skip = (nextPages - 1) * FEED_PAGE_SIZE + offset;

    const res = await getFeed(limit, skip, async () => {
      const loginState = await authFlow();
      if (loginState === LoginState.IN) {
        return true;
      }
      return false;
    });

    if (res.error) {
      setFeedState({ ...feedState, state: FeedState.ERROR });
      return;
    } else if (!res.feed.length) {
      setFeedState({
        ...feedState,
        state: FeedState.READY
      });
      return;
    }

    const nextTotal = feedState.total + res.feed.length;
    const nextDataProvider = dataProviderWithData(
      feedState.dataProvider,
      res.feed,
      refresh
    );

    setFeedState({
      state: FeedState.READY,
      pages: nextPages,
      total: nextTotal,
      dataProvider: nextDataProvider
    });
  };

  return {
    feedState: feedState.state,
    addPage,
    dataProvider: feedState.dataProvider
  };
};

const FeedContainer = createContainer(useFeed);
export default FeedContainer;
