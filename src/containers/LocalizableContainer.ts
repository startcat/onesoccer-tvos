import { NativeModules } from "react-native";
import { createContainer } from "unstated-next";

export type LocalizableProps = {
  LOGIN_INPUT_USERNAME: string;
  LOGIN_INPUT_PASSWORD: string;
  LOGIN_BUTTON_LOGIN: string;
  LOGIN_EMPTY_FIELDS: string;
  LOGOUT: string;
  LOGOUT_CONFIRM: string;
  ERROR_STANDARD: string;
  ERROR_UNAUTHORIZED: string;
  ERROR_WRONG_CREDENTIALS: string;
  LOAD_MORE: string;
  REFRESH_FEED: string;
  "24H": string;
  LIVE: string;
  YES: string;
  NO: string;
  SCHEDULE: string;
};

const props: LocalizableProps = NativeModules.LocalizableManager;

const LocalizableContainer = createContainer(() => {
  return props;
});

export default LocalizableContainer;
