import { createContainer } from "unstated-next";
import { useState, useEffect } from "react";
import { AppStateStatus, AppState } from "react-native";

export enum ApplicationState {
  FOREGROUND = 0,
  BACKGROUND
}

const ApplicationStateContainer = createContainer(() => {
  const [applicationState, setAppState] = useState(ApplicationState.FOREGROUND);

  useEffect(() => {
    const handler = async (status: AppStateStatus) => {
      if (status === "active") {
        setAppState(ApplicationState.FOREGROUND);
      } else if (status === "inactive") {
        setAppState(ApplicationState.BACKGROUND);
      }
    };
    AppState.addEventListener("change", handler);

    return () => {
      AppState.removeEventListener("change", handler);
    };
  }, []);

  return { applicationState };
});

export default ApplicationStateContainer;
