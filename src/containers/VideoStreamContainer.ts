import { useState } from "react";

import { createContainer } from "unstated-next";

import { FeedItem } from "@internal/network/services/getFeed";
import getVideoInfo, {
  VideoInfo
} from "@internal/network/services/getVideoInfo";

import getVideoManifest, {
  VideoManifest
} from "@internal/network/services/getVideoManifest";

import { getAuthData } from "@internal/libs/keychain";

import pushVideoScreen from "@internal/screens/pushVideoScreen";
import { authFlow, LoginState } from "@internal/containers/SessionContainer";
import getSettings from "@internal/network/services/getSettings";

export enum VideoStreamState {
  IDLE = 0,
  FETCHING,
  ERROR,
  ERROR_UNAUTHORIZED
}

export type VideoStream = FeedItem &
  VideoInfo &
  VideoManifest & { sessionUUID: string };

const RETRY_CALLBACK = async () => {
  const loginState = await authFlow();
  if (loginState === LoginState.IN) {
    return true;
  }
  return false;
};

const useVideoStream = () => {
  const [videoStreamState, setVideoStreamState] = useState<VideoStreamState>(
    VideoStreamState.IDLE
  );

  const setLiveVideo = async () => {
    if (videoStreamState === VideoStreamState.FETCHING) {
      return;
    }

    setVideoStreamState(VideoStreamState.FETCHING);

    const settings = await getSettings();
    if (settings.error) {
      return VideoStreamState.ERROR;
    }

    // http://ac.start.cat/projects/58/discussions?modal=Discussion-310-58

    const feedItem: FeedItem = {
      id: settings.channel.selectedStream,
      title: settings.channel.name,
      live: true,
      altId: "",
      date: "",
      header: "",
      images: {
        landscape: "",
        thumb: ""
      }
    };

    const authData = await getAuthData();
    if (!authData) {
      // TODO (but this should not happen, since is keychain)
      return;
    }

    const videoInfo: VideoInfo = {
      live: true,
      uid: "",
      selectedStream: settings.channel.selectedStream
    };

    const videoManifest = await getVideoManifest(
      videoInfo,
      authData.uuid,
      RETRY_CALLBACK
    );
    if (videoManifest.error) {
      if (videoManifest.status === 403) {
        return setVideoStreamState(VideoStreamState.ERROR_UNAUTHORIZED);
      }
      return setVideoStreamState(VideoStreamState.ERROR);
    }

    const userData = { sessionUUID: authData.uuid };

    const videoStream = {
      ...feedItem,
      ...videoInfo,
      ...videoManifest,
      ...userData
    };

    setVideoStreamState(VideoStreamState.IDLE);

    pushVideoScreen(videoStream);
  };

  const setVideo = async (feedItem: FeedItem) => {
    if (videoStreamState === VideoStreamState.FETCHING) {
      return;
    }

    setVideoStreamState(VideoStreamState.FETCHING);

    const videoInfo = await getVideoInfo(feedItem, RETRY_CALLBACK);
    if (videoInfo.error) {
      if (videoInfo.status === 403) {
        return setVideoStreamState(VideoStreamState.ERROR_UNAUTHORIZED);
      }
      return setVideoStreamState(VideoStreamState.ERROR);
    }

    const authData = await getAuthData();
    if (!authData) {
      // TODO (but this should not happen, since is keychain)
      return;
    }

    const videoManifest = await getVideoManifest(
      videoInfo,
      authData.uuid,
      RETRY_CALLBACK
    );
    if (videoManifest.error) {
      if (videoManifest.status === 403) {
        return setVideoStreamState(VideoStreamState.ERROR_UNAUTHORIZED);
      }
      return setVideoStreamState(VideoStreamState.ERROR);
    }

    const userData = { sessionUUID: authData.uuid };

    const videoStream = {
      ...feedItem,
      ...videoInfo,
      ...videoManifest,
      ...userData
    };

    setVideoStreamState(VideoStreamState.IDLE);

    pushVideoScreen(videoStream);
  };

  return { videoStreamState, setVideo, setLiveVideo };
};

const VideoSreamContainer = createContainer(useVideoStream);

export default VideoSreamContainer;
