import {
  getUserCredentials,
  setUserCredentials,
  getAuthData,
  setAuthData,
  clearKeychainData
} from "@internal/libs/keychain";

import { createContainer } from "unstated-next";

import doLogin from "@internal/network/services/doLogin";
import getSettings from "@internal/network/services/getSettings";
import putUser from "@internal/network/services/putUser";
import { useState } from "react";
import authSession from "@internal/network/services/authSession";

export enum LoginState {
  UNKKNOWN = 0,
  FETCHING,
  IN,
  OUT,
  WRONG_CREDENTIALS,
  ERROR,
  ERROR_BUT_LOGGED_IN,
  NEEDS_LOGOUT
}

const isAuthError = (code: number) => code === 401 || code === 403;

const loginFlow = async (
  username: string,
  password: string
): Promise<LoginState> => {
  const loginRes = await doLogin(username, password);
  if (loginRes.error) {
    return loginRes.status === 401
      ? LoginState.WRONG_CREDENTIALS
      : LoginState.ERROR;
  }

  const { uuid, token } = loginRes;

  const putUserRes = await putUser(username, uuid, token);
  if (putUserRes.error) {
    return LoginState.ERROR;
  }

  // everything ok, we're in

  await setUserCredentials(username, password);
  await setAuthData(uuid, token);

  return LoginState.IN;
};

export const authFlow = async (): Promise<LoginState> => {
  const settings = await getSettings();
  if (settings.error) {
    return LoginState.ERROR;
  }

  const credentials = await getUserCredentials();
  if (!credentials) {
    return LoginState.OUT;
  }

  const authData = await getAuthData();
  if (!authData) {
    return LoginState.OUT;
  }

  // fake "login", just need this to configure agent plugins
  await doLogin(credentials.username, credentials.password, authData);

  const authSessionRes = await authSession(authData.uuid);
  if (!authSessionRes.error) {
    return LoginState.IN;
  } else if (authSessionRes.error) {
    if (!isAuthError(authSessionRes.status)) {
      return LoginState.ERROR_BUT_LOGGED_IN;
    }
  }

  return await loginFlow(credentials.username, credentials.password);
};

const useSession = () => {
  const [loginState, setLoginState] = useState<LoginState>(LoginState.UNKKNOWN);

  // setup

  const setup = async () => {
    const nextLoginState = await authFlow();
    if (nextLoginState === LoginState.OUT) {
      await clearKeychainData();
    }

    setLoginState(nextLoginState);
  };

  // login (aka setCredentials)

  const setCredentials = async (username: string, password: string) => {
    if (loginState === LoginState.FETCHING) {
      return;
    }

    setLoginState(LoginState.FETCHING);

    return setLoginState(await loginFlow(username, password));
  };

  // logout flow

  const setNeedsLogout = () => setLoginState(LoginState.NEEDS_LOGOUT);

  const removeCredentials = async () => {
    if (loginState === LoginState.OUT) {
      return;
    }

    await clearKeychainData();

    setLoginState(LoginState.OUT);
  };

  return {
    setup,
    loginState,
    setCredentials,
    setNeedsLogout,
    removeCredentials,
    setLoginState
  };
};

const SessionContainer = createContainer(useSession);

export default SessionContainer;
