import { useState } from "react";

import { createContainer } from "unstated-next";

import { getDayOfYear, parseISO, getYear, compareAsc, format } from "date-fns";

import getSchedule, {
  ScheduleResponseItem
} from "@internal/network/services/getSchedule";
import getSettings from "@internal/network/services/getSettings";

export enum ScheduleFetchState {
  IDLE = 0,
  FETCHING,
  FETCHED,
  ERROR
}

export type ScheduleRow = {
  title: string;
  dateTitle: string;
  isLive: boolean;
};

export type ScheduleDay = {
  title: string;
  items: Array<ScheduleRow>;
};

type MapperItem = {
  title: string;
  date: Date;
  sort: number;
  live: number;
};

const mapper = (item: ScheduleResponseItem): MapperItem => {
  const { title, date, live } = item;

  const d = parseISO(date);
  const sort = getYear(d) * 1000 + getDayOfYear(d);

  return { title, date: d, sort, live };
};

const sorter = (a: MapperItem, b: MapperItem) => compareAsc(a.date, b.date);

const reducer = (
  acc: Array<ScheduleDay>,
  item: MapperItem,
  idx: number,
  data: Array<MapperItem>
) => {
  if (idx === 0 || data[idx - 1].sort !== item.sort) {
    acc.push({
      title: format(item.date, "EEE dd"),
      items: []
    });
  }

  const { title, date, live } = item;
  const dateTitle = format(date, "hh:mmaaa");

  acc[acc.length - 1].items.push({ title, dateTitle, isLive: live === 1 });
  return acc;
};

const ScheduleContainer = createContainer(() => {
  const [fetchState, setFetchState] = useState<ScheduleFetchState>(
    ScheduleFetchState.IDLE
  );
  const [scheduleItems, setSceduleItems] = useState<
    Array<ScheduleDay> | undefined
  >();

  const fetch = async () => {
    if (fetchState === ScheduleFetchState.FETCHING) {
      return;
    }

    setFetchState(ScheduleFetchState.FETCHING);

    const settings = await getSettings();
    if (settings.error) {
      return setFetchState(ScheduleFetchState.ERROR);
    }

    const schedule = await getSchedule(settings.epg);
    if (schedule.error) {
      return setFetchState(ScheduleFetchState.ERROR);
    }

    const items = schedule.data
      .map(mapper)
      .sort(sorter)
      .reduce<Array<ScheduleDay>>(reducer, []);

    setSceduleItems(items);
    setFetchState(ScheduleFetchState.FETCHED);
  };

  return { fetch, fetchState, scheduleItems };
});
export default ScheduleContainer;
