import {
  getGenericPassword,
  setGenericPassword,
  resetGenericPassword
} from "react-native-keychain";

type Credentials =
  | {
      username: string;
      password: string;
    }
  | false;

export const getUserCredentials = async (): Promise<Credentials> => {
  try {
    return await getGenericPassword();
  } catch (err) {}
  return false;
};

export const setUserCredentials = async (
  username: string,
  password: string
): Promise<boolean> => {
  try {
    await setGenericPassword(username, password);
    return true;
  } catch (err) {}

  return false;
};

// https://github.com/oblador/react-native-keychain/issues/36

const AUTH_DATA_SERVICE = {
  service: "$AUTH_DATA"
};

export const setAuthData = async (
  uuid: string,
  token: string = ""
): Promise<void> => {
  try {
    await setGenericPassword(uuid, token, AUTH_DATA_SERVICE);
  } catch (err) {}
};

type AuthData = { uuid: string; token: string };

export const getAuthData = async (): Promise<AuthData | false> => {
  try {
    const result = await getGenericPassword(AUTH_DATA_SERVICE);
    if (!result) {
      return false;
    }
    const { username, password } = result;
    return { uuid: username, token: password };
  } catch (err) {}
  return false;
};

export const clearKeychainData = async () => {
  await resetGenericPassword();
  await resetGenericPassword(AUTH_DATA_SERVICE);
};
