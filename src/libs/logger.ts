import log from "loglevel";
import { LOG_LEVEL } from "react-native-dotenv";

switch (LOG_LEVEL) {
  case "trace":
    log.setLevel(log.levels.TRACE);
    break;
  case "debug":
    log.setLevel(log.levels.DEBUG);
    break;
  case "info":
    log.setLevel(log.levels.INFO);
    break;
  case "warn":
    log.setLevel(log.levels.WARN);
    break;
  case "error":
    log.setLevel(log.levels.ERROR);
    break;
  default:
    log.setLevel(log.levels.SILENT);
}

const logger = log;

export default logger;
