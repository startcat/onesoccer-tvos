import Toast from "react-native-root-toast";
import typography from "@internal/styles/typography";
import constants from "@internal/styles/constants";
import colors from "@internal/styles/colors";

let currentToast: React.Component | null = null;

export const makeToast = (message: string, withDelay: boolean = false) => {
  currentToast = Toast.show(message, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    textStyle: typography.circular,
    containerStyle: {
      padding: constants.SPACING,
      backgroundColor: colors.ERROR_RED,
      marginBottom: constants.SPACING * 2
    },
    textColor: colors.WHITE,
    animation: true,
    delay: !withDelay ? 0 : constants.ANIMATION_DURATION * 2,
    onHidden: () => (currentToast = null),
    hideOnPress: false,
    shadow: false
  });
};

export const clearToast = () => {
  if (currentToast !== null) {
    Toast.hide(currentToast);
    currentToast = null;
  }
};
