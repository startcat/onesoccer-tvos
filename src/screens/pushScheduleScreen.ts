import { pushNativeScreen } from "@internal/nativeBindings/pushNativeScreen";

const pushScheduleScreen = () => pushNativeScreen("ScheduleScreen");

export default pushScheduleScreen;
