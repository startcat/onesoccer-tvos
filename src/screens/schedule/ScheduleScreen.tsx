import React, { useRef, useEffect, useState } from "react";
import { StyleSheet, ScrollView, View, Text } from "react-native";

import {
  Transition,
  Transitioning,
  TransitioningView
} from "react-native-reanimated";

import { ScreenComponent } from "@internal/screens/types";
import colors from "@internal/styles/colors";
import ScheduleContainer, {
  ScheduleFetchState
} from "@internal/containers/ScheduleContainer";
import constants from "@internal/styles/constants";
import { WhiteActivityIndicator } from "@internal/components/WhiteActivityIndicator";
import { makeToast } from "@internal/libs/toast";
import LocalizableContainer from "@internal/containers/LocalizableContainer";
import { FocusableButtonView } from "@internal/nativeBindings/FocusableButtonView";
import typography, { BODY_SIZE } from "@internal/styles/typography";

// transitioning between states

const transition = (
  <Transition.Sequence>
    <Transition.Out type="fade" durationMs={constants.ANIMATION_DURATION} />
    <Transition.In type="fade" durationMs={constants.ANIMATION_DURATION} />
  </Transition.Sequence>
);

// screen component

const ScheduleScreen: ScreenComponent = () => {
  const { scheduleItems, fetchState, fetch } = ScheduleContainer.useContainer();
  const strings = LocalizableContainer.useContainer();

  if (fetchState === ScheduleFetchState.IDLE) {
    fetch();
  } else if (fetchState === ScheduleFetchState.ERROR) {
    makeToast(strings.ERROR_STANDARD, true);
  }

  const transitioningViewRef = useRef<TransitioningView>(null);
  const [currentDayIndex, setCurrentDayIndex] = useState(0);

  useEffect(() => {
    transitioningViewRef &&
      transitioningViewRef.current &&
      transitioningViewRef.current.animateNextTransition();
  }, [fetchState, currentDayIndex]);

  return (
    <Transitioning.View
      transition={transition}
      ref={transitioningViewRef}
      style={styles.container}
    >
      {fetchState === ScheduleFetchState.FETCHING && (
        <WhiteActivityIndicator style={styles.activityIndicator} />
      )}
      {fetchState === ScheduleFetchState.FETCHED && scheduleItems && (
        <View style={styles.main}>
          <View style={styles.scheduleMenu}>
            {scheduleItems.map((item, idx) => {
              return (
                <FocusableButtonView
                  key={`scheduleMenuDay$${idx}`}
                  focusedColor={colors.FOCUS_HIGHLIGHT0}
                  onPress={() => setCurrentDayIndex(idx)}
                >
                  <View
                    style={[
                      styles.scheduleMenuItem,
                      idx === currentDayIndex && styles.scheduleMenuItemSelected
                    ]}
                  >
                    <Text style={[typography.brand1, styles.scheduleMenuText]}>
                      {item.title}
                    </Text>
                  </View>
                </FocusableButtonView>
              );
            })}
          </View>
          <ScrollView
            key={`scheduleListDay$${currentDayIndex}`}
            contentContainerStyle={styles.scrollView}
          >
            {scheduleItems[currentDayIndex].items.map((item, idx) => {
              return (
                <FocusableButtonView
                  key={`scheduleListDay$${currentDayIndex}Item$${idx}`}
                  focusedColor={colors.FOCUS_OPACITY0}
                >
                  <View
                    style={[
                      styles.scheduleDayRow,
                      idx === 0 && styles.scheduleDayRow0
                    ]}
                  >
                    <View style={styles.scheduleDayRowTime}>
                      <Text
                        style={[typography.circular, styles.scheduleDayRowText]}
                      >
                        {item.dateTitle}
                      </Text>
                    </View>
                    <View style={styles.scheduleDayRowTitle}>
                      <Text
                        style={[typography.brand1, styles.scheduleDayRowText]}
                      >
                        {item.title}
                      </Text>
                    </View>
                  </View>
                  {item.isLive && (
                    <View style={styles.live}>
                      <Text style={[typography.brand2, styles.liveText]}>
                        {strings.LIVE}
                      </Text>
                    </View>
                  )}
                </FocusableButtonView>
              );
            })}
          </ScrollView>
        </View>
      )}
    </Transitioning.View>
  );
};

const MENU_HEIGHT = 200;
const VPADDING = 92;
const HPADDING = 80;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: colors.PASSION
  },
  activityIndicator: {
    alignSelf: "center"
  },
  main: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center"
  },
  scheduleMenu: {
    width: "100%",
    height: MENU_HEIGHT,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.NIGHT
  },
  scheduleMenuItem: {
    paddingVertical: VPADDING / 4,
    paddingHorizontal: HPADDING / 2,
    borderBottomWidth: 8,
    borderBottomColor: colors.NIGHT
  },
  scheduleMenuItemSelected: {
    borderBottomColor: colors.PASSION
  },
  scrollView: {
    alignSelf: "stretch",
    marginHorizontal: HPADDING
  },
  scheduleMenuText: {
    color: colors.WHITE,
    fontSize: BODY_SIZE * 2,
    textTransform: "uppercase"
  },
  scheduleDayRow: {
    borderTopColor: colors.WHITE,
    borderTopWidth: 2,
    paddingVertical: VPADDING,
    paddingHorizontal: HPADDING / 2
  },
  scheduleDayRow0: {
    borderTopWidth: 0
  },
  scheduleDayRowTime: {
    position: "absolute",
    top: VPADDING,
    left: HPADDING / 2
  },
  scheduleDayRowTitle: {
    marginLeft: 260
  },
  scheduleDayRowText: {
    color: colors.WHITE,
    fontSize: BODY_SIZE * 2,
    textTransform: "uppercase",
    lineHeight: BODY_SIZE * 2 + Math.round(constants.SPACING / 4)
  },
  live: {
    position: "absolute",
    padding: 10,
    left: HPADDING / 2,
    backgroundColor: colors.WHITE
  },
  liveText: {
    fontSize: BODY_SIZE,
    textTransform: "uppercase",
    color: colors.PASSION
  }
});

export default ScheduleScreen;
