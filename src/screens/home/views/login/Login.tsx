import React, { useState } from "react";

import {
  View,
  Image,
  StyleSheet,
  ViewStyle,
  ImageStyle,
  ViewProps
} from "react-native";

import typography from "@internal/styles/typography";
import constants from "@internal/styles/constants";
import colors from "@internal/styles/colors";
import LocalizableContainer from "@internal/containers/LocalizableContainer";
import { makeToast } from "@internal/libs/toast";
import { StandardButton } from "@internal/components/StandardButton";
import { TVTextFieldView } from "@internal/nativeBindings/TVTextFieldView";

type LoginViewProps = {
  onSubmit: (username: string, password: string) => void;
};

const LoginView: React.FunctionComponent<
  LoginViewProps & ViewProps
> = props => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const strings = LocalizableContainer.useContainer();

  const onPress = () => {
    if (!username.length || !password.length) {
      makeToast(strings.LOGIN_EMPTY_FIELDS);
      return;
    }
    props.onSubmit(username, password);
  };

  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        source={require("@internal/assets/images/logos/logo.png")}
      />
      <TVTextFieldView
        style={[styles.input]}
        font={typography.circular}
        textValue={username}
        placeholderValue={strings.LOGIN_INPUT_USERNAME}
        onChangeText={({ nativeEvent: { text } }) => setUsername(text)}
      />
      <TVTextFieldView
        style={[styles.input]}
        font={typography.circular}
        textValue={password}
        placeholderValue={strings.LOGIN_INPUT_PASSWORD}
        secureTextEntry={true}
        onChangeText={({ nativeEvent: { text } }) => {
          setPassword(text);
          if (text.length && username.length) {
            props.onSubmit(username, password);
          }
        }}
      />
      <StandardButton
        title={strings.LOGIN_BUTTON_LOGIN.toUpperCase()}
        onPress={onPress}
      />
    </View>
  );
};

type ComponentStyle = {
  container: ViewStyle;
  input: ViewStyle;
  button: ViewStyle;
  logo: ImageStyle;
};

const styles = StyleSheet.create<ComponentStyle>({
  container: {
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 265,
    height: 128,
    marginBottom: 5 * constants.SPACING
  },
  input: {
    ...constants.BUTTON_SIZE,
    marginBottom: constants.SPACING,
    borderRadius: 6
  },
  button: {
    ...constants.BUTTON_SIZE,
    backgroundColor: colors.NIGHT,
    borderRadius: 6,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default LoginView;
