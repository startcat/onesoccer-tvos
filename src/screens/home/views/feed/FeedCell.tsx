import React from "react";

import { Text, StyleSheet, ViewStyle, TextStyle, View } from "react-native";

import { FeedItem } from "@internal/network/services/getFeed";
import { ViewComponent } from "@internal/components/types";
import VideoSreamContainer from "@internal/containers/VideoStreamContainer";
import { SmartFocusView } from "@internal/nativeBindings/SmartFocusView";
import typography from "@internal/styles/typography";
import constants from "@internal/styles/constants";
import colors from "@internal/styles/colors";
import {
  THUMB_WIDTH,
  THUMB_HEIGHT
} from "@internal/screens/home/views/feed/constants";
import { ParallaxImageView } from "@internal/nativeBindings/ParallaxView";
import LocalizableContainer from "@internal/containers/LocalizableContainer";

type FeedRowProps = {
  data: FeedItem;
  isFirstCell: boolean;
};

const FeedCell: ViewComponent<FeedRowProps> = props => {
  const strings = LocalizableContainer.useContainer();
  const { setVideo } = VideoSreamContainer.useContainer();

  const { data, isFirstCell } = props;

  const onPress = () => {
    setVideo(data);
  };

  return (
    <SmartFocusView
      style={styles.cellContainer}
      needsFocusGuide={true}
      claimsFirstFocus={isFirstCell}
    >
      <ParallaxImageView
        imageUrl={data.images.landscape}
        style={styles.banner}
        cornerRadius={12}
        onPress={onPress}
      >
        {data.live && (
          <View style={styles.bannerLiveContainer}>
            <Text style={[typography.brand2, styles.bannerLiveText]}>
              {strings.LIVE}
            </Text>
          </View>
        )}
      </ParallaxImageView>
      <View style={styles.info}>
        <Text style={[typography.brand2, styles.infoHeader]}>
          {data.header}
        </Text>
        <Text
          style={[typography.brand1, styles.infoTitle]}
          numberOfLines={2}
          ellipsizeMode="tail"
        >
          {data.title}
        </Text>
      </View>
    </SmartFocusView>
  );
};

type FeedRowStyle = {
  cellContainer: ViewStyle;
  banner: ViewStyle;
  bannerLiveContainer: ViewStyle;
  bannerLiveText: TextStyle;
  info: ViewStyle;
  infoHeader: TextStyle;
  infoTitle: TextStyle;
};

const styles = StyleSheet.create<FeedRowStyle>({
  cellContainer: {
    height: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  banner: {
    width: THUMB_WIDTH,
    height: THUMB_HEIGHT,
    marginTop: constants.SPACING * 3,
    backgroundColor: colors.LIGHT_GREY
  },
  bannerLiveContainer: {
    position: "absolute",
    top: 0,
    right: 0,
    paddingVertical: constants.SPACING / 2,
    paddingHorizontal: constants.SPACING,
    backgroundColor: colors.PASSION,
    borderTopRightRadius: 12
  },
  bannerLiveText: {
    color: colors.WHITE,
    textTransform: "uppercase",
    fontSize: 14
  },
  info: {
    width: THUMB_WIDTH,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    marginTop: constants.SPACING
  },
  infoHeader: {
    color: colors.PASSION,
    textTransform: "uppercase",
    fontSize: 18
  },
  infoTitle: {
    marginTop: 5,
    color: colors.WHITE,
    fontSize: 28
  }
});

export default FeedCell;
