import React from "react";
import { ViewStyle, StyleSheet, View } from "react-native";

import { ViewComponent } from "@internal/components/types";

import LocalizableContainer from "@internal/containers/LocalizableContainer";
import SessionContainer from "@internal/containers/SessionContainer";
import { UserMenuItem } from "@internal/screens/home/views/feed/UserMenuItem";
import colors from "@internal/styles/colors";
import VideoSreamContainer from "@internal/containers/VideoStreamContainer";
import pushScheduleScreen from "@internal/screens/pushScheduleScreen";

type UserMenuProps = { isShown: boolean };
const UserMenu: ViewComponent<UserMenuProps> = props => {
  const strings = LocalizableContainer.useContainer();
  const { setNeedsLogout } = SessionContainer.useContainer();
  const { setLiveVideo } = VideoSreamContainer.useContainer();

  const on24h = () => setLiveVideo();
  const onLogout = () => setNeedsLogout();

  return (
    <View style={styles.container}>
      <UserMenuItem
        title={strings["24H"]}
        iconName="logo"
        iconSmaller={false}
        style={styles.item}
        color={colors.WHITE}
        fullSize={props.isShown}
        onPress={on24h}
      />
      <UserMenuItem
        title={strings.SCHEDULE}
        iconName="calendar"
        iconSmaller={true}
        style={styles.item}
        color={colors.WHITE}
        fullSize={props.isShown}
        onPress={pushScheduleScreen}
      />
      <UserMenuItem
        title={strings.LOGOUT}
        iconName="logout"
        iconSmaller={true}
        style={styles.item}
        color={colors.WHITE}
        fullSize={props.isShown}
        onPress={onLogout}
      />
    </View>
  );
};

type UserMenuStyle = {
  container: ViewStyle;
  item: ViewStyle;
};

const styles = StyleSheet.create<UserMenuStyle>({
  container: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    alignItems: "flex-end",
    justifyContent: "center",
    overflow: "hidden"
  },
  item: {
    width: "100%"
  }
});

export default UserMenu;
