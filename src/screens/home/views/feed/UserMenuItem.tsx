import React from "react";

import { Text, ViewStyle, TextStyle, StyleSheet } from "react-native";
import { ViewComponent } from "@internal/components/types";

import Icon from "@internal/styles/icon-font";
import {
  MENU_ICON_SIZE,
  MENU_ICON_SIZE_FOLDED,
  MENU_ICON_SIZE_FOLDED_DIFF,
  MENU_ICON_SIZE_UNFOLDED,
  MENU_ICON_SIZE_UNFOLDED_DIFF,
  MENU_FOLDED_WIDTH,
  MENU_UNFOLDED_WIDTH,
  MENU_TITLE_SIZE,
  MENU_INNER_PADDING,
  MENU_PADDING
} from "@internal/screens/home/views/feed/constants";

import typography from "@internal/styles/typography";
import { FocusableButtonView } from "@internal/nativeBindings/FocusableButtonView";
import colors from "@internal/styles/colors";

const buttonStyleFolded = {
    width: MENU_FOLDED_WIDTH
};

const buttonStyleUnfolded = {
    width: MENU_UNFOLDED_WIDTH
};

export type UserMenuItemProps = {
  iconName: string;
  iconSmaller: boolean;
  fullSize: boolean;
  title: string;
  color: string;
  onPress: () => void;
};

export const UserMenuItem: ViewComponent<UserMenuItemProps> = props => {
  const { style } = props;
  return (
    <FocusableButtonView
      focusedColor={colors.NIGHT}
      style={[
        style,
        styles.buttonContainer,
        !props.fullSize ? buttonStyleFolded : buttonStyleUnfolded
      ]}
      onPress={props.onPress}
    >
      <Icon name={props.iconName} 
        size={!props.fullSize ? (props.iconSmaller ? MENU_ICON_SIZE_FOLDED - MENU_ICON_SIZE_FOLDED_DIFF : MENU_ICON_SIZE_FOLDED) : (props.iconSmaller ? MENU_ICON_SIZE_UNFOLDED - MENU_ICON_SIZE_UNFOLDED_DIFF : MENU_ICON_SIZE_UNFOLDED)} 
        color={props.color} 
        style={!props.fullSize ? (props.iconSmaller ? styles.iconFoldedSmaller : styles.iconFolded) : (props.iconSmaller ? styles.iconUnFoldedSmaller : styles.iconUnFolded)} />
      {props.fullSize && (
        <Text
          style={[typography.brand2, styles.iconTitle, { color: props.color }]}
        >
          {props.title}
        </Text>
      )}
    </FocusableButtonView>
  );
};

type UserMenuStyle = {
  buttonContainer: ViewStyle;
  icon: ViewStyle;
  iconFolded: ViewStyle;
  iconFoldedSmaller: ViewStyle;
  iconUnFolded: ViewStyle;
  iconUnFoldedSmaller: ViewStyle;
  iconTitle: TextStyle;
};

const styles = StyleSheet.create<UserMenuStyle>({
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: MENU_PADDING,
    paddingRight: MENU_PADDING,
    height: 90
  },
  icon: {},
  iconFolded: {
    
  },
  iconFoldedSmaller: {
    marginLeft: MENU_ICON_SIZE_FOLDED_DIFF / 2
  },
  iconUnFolded: {
    
  },
  iconUnFoldedSmaller: {
    marginLeft: MENU_ICON_SIZE_UNFOLDED_DIFF / 2
  },
  iconTitle: {
    width: MENU_TITLE_SIZE,
    marginLeft: MENU_INNER_PADDING,
    textTransform: "uppercase"
  }
});
