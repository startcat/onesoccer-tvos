import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

import constants from "@internal/styles/constants";

import { FEED_COLUMNS } from "@internal/config";

export const CELLS_PER_ROW = FEED_COLUMNS;

// menu

export const MENU_ICON_SIZE = 40;
export const MENU_ICON_SIZE_FOLDED = 56;
export const MENU_ICON_SIZE_FOLDED_DIFF = 14;
export const MENU_ICON_SIZE_UNFOLDED = 32;
export const MENU_ICON_SIZE_UNFOLDED_DIFF = 8;
export const MENU_TITLE_SIZE = 170;
export const MENU_PADDING = constants.SPACING * 2;
export const MENU_INNER_PADDING = constants.SPACING;

export const MENU_FOLDED_WIDTH = MENU_PADDING + MENU_ICON_SIZE_FOLDED + MENU_PADDING;
export const MENU_UNFOLDED_WIDTH = MENU_PADDING + MENU_ICON_SIZE_UNFOLDED + MENU_INNER_PADDING + MENU_TITLE_SIZE + MENU_PADDING;

// cell

export const CELL_PADDING = constants.SPACING * 3;

export const CELL_WIDTH = Math.floor(
  (width - MENU_FOLDED_WIDTH - CELL_PADDING * 2) / CELLS_PER_ROW
);

export const CELL_HEIGHT = Math.round(height / 3.25);

//  parent                            cells                                         parent
//   _____  _______________________________________________________________________  ____
// || CP  || CP + THUMB + CP | CP + THUMB + CP | CP + THUMB + CP | CP + THUMB + CP || CP ||

export const THUMB_WIDTH = CELL_WIDTH - CELL_PADDING * 2;
// image original size is 828x466
export const THUMB_HEIGHT = Math.round(THUMB_WIDTH * (466 / 828));
