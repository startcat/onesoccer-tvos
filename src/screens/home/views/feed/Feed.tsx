import React, { useEffect, useRef, useState } from "react";

import { StyleSheet, ViewStyle } from "react-native";

import { RecyclerListView, LayoutProvider } from "recyclerlistview";

import {
  Transitioning,
  Transition,
  TransitioningView
} from "react-native-reanimated";

import { FeedItem } from "@internal/network/services/getFeed";
import { ViewComponent } from "@internal/components/types";
import FeedContainer, { FeedState } from "@internal/containers/FeedContainer";

import FeedCell from "@internal/screens/home/views/feed/FeedCell";
import {
  CELL_WIDTH,
  CELLS_PER_ROW,
  MENU_FOLDED_WIDTH,
  CELL_HEIGHT,
  CELL_PADDING
} from "@internal/screens/home/views/feed/constants";

import UserMenu from "@internal/screens/home/views/feed/UserMenu";
import {
  SmartFocusEvent,
  SmartFocusView
} from "@internal/nativeBindings/SmartFocusView";
import LocalizableContainer from "@internal/containers/LocalizableContainer";
import constants from "@internal/styles/constants";
import ApplicationStateContainer, {
  ApplicationState
} from "@internal/containers/ApplicationStateContainer";
import colors from "@internal/styles/colors";
import { WhiteActivityIndicator } from "@internal/components/WhiteActivityIndicator";
import { OutlinedButton } from "@internal/components/OutlinedButton";

// TODO: refresh on app state change

const rowRenderer = (_type: any, item: FeedItem, index: number) => {
  return <FeedCell data={item} isFirstCell={index === 0} />;
};

const layoutProvider = new LayoutProvider(
  () => {
    return 0;
  },
  (_type, dim) => {
    dim.width = CELL_WIDTH;
    dim.height = CELL_HEIGHT;
  }
);

const transition = <Transition.Change interpolation="easeInOut" />;

const FeedView: ViewComponent = props => {
  const strings = LocalizableContainer.useContainer();
  const { dataProvider, addPage, feedState } = FeedContainer.useContainer();
  const { applicationState } = ApplicationStateContainer.useContainer();

  const transitioningViewRef = useRef<TransitioningView>(null);
  const [isMenuShown, setIsMenuShown] = useState(false);

  // refresh logic

  useEffect(() => {
    if (applicationState === ApplicationState.FOREGROUND) {
      addPage(true);
    }
  }, [applicationState]);

  // menu layouting and animations

  const onMenuFocusDidChange: SmartFocusEvent = ev => {
    transitioningViewRef &&
      transitioningViewRef.current &&
      transitioningViewRef.current.animateNextTransition();

    setIsMenuShown(ev.nativeEvent.isFocused);
  };

  // render

  const onShouldLoadMore = () => addPage();
  const showsButton = feedState !== FeedState.FETCHING;

  const LoadMoreButton = () => {
    return (
      <SmartFocusView
        style={styles.loadMoreButtonContainer}
        doNotStorePreferredSmartFocus={true}
        needsFocusGuide={true}
      >
        {!showsButton && <WhiteActivityIndicator />}
        {showsButton && (
          <OutlinedButton
            title={strings.LOAD_MORE.toUpperCase()}
            onPress={onShouldLoadMore}
          />
        )}
      </SmartFocusView>
    );
  };

  const { style } = props;

  return (
    (dataProvider !== null && (
      <Transitioning.View
        ref={transitioningViewRef}
        transition={transition}
        style={[style, styles.container]}
      >
        <SmartFocusView
          style={[styles.menu]}
          areaType="menu"
          needsFocusGuide={true}
          onFocusDidChange={onMenuFocusDidChange}
        >
          <UserMenu isShown={isMenuShown} />
        </SmartFocusView>
        <SmartFocusView style={styles.list} areaType="main">
          <RecyclerListView
            layoutProvider={layoutProvider}
            dataProvider={dataProvider}
            rowRenderer={rowRenderer}
            renderFooter={LoadMoreButton}
          />
        </SmartFocusView>
      </Transitioning.View>
    )) ||
    null
  );
};

type FeedStyle = {
  container: ViewStyle;
  menu: ViewStyle;
  list: ViewStyle;
  loadMoreButton: ViewStyle;
  loadMoreButtonContainer: ViewStyle;
};

const styles = StyleSheet.create<FeedStyle>({
  container: {
    flexDirection: "row",
    backgroundColor: colors.NIGHT
  },
  menu: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    backgroundColor: colors.PASSION,
    zIndex: 10
  },
  list: {
    top: 0,
    left: MENU_FOLDED_WIDTH,
    paddingHorizontal: CELL_PADDING,
    right: 0,
    bottom: 0,
    position: "absolute",
    zIndex: 1
  },
  loadMoreButtonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: CELL_WIDTH * CELLS_PER_ROW,
    paddingVertical: CELL_PADDING
  },
  loadMoreButton: {
    ...constants.BUTTON_SIZE,
    backgroundColor: colors.PASSION,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default FeedView;
