import React, { useRef, useState, useEffect } from "react";
import { StyleSheet, ViewStyle, View, Alert } from "react-native";

import {
  Transitioning,
  Transition,
  TransitioningView
} from "react-native-reanimated";

import LoginView from "@internal/screens/home/views/login/Login";
import FeedView from "@internal/screens/home/views/feed/Feed";
import { ScreenComponent } from "@internal/screens/types";
import { BlurredLoadingView } from "@internal/components/BlurredLoadingView";

import SessionContainer, {
  LoginState
} from "@internal/containers/SessionContainer";
import VideoSreamContainer, {
  VideoStreamState
} from "@internal/containers/VideoStreamContainer";

import { makeToast, clearToast } from "@internal/libs/toast";
import LocalizableContainer from "@internal/containers/LocalizableContainer";
import constants from "@internal/styles/constants";
import colors from "@internal/styles/colors";
import { WhiteActivityIndicator } from "@internal/components/WhiteActivityIndicator";

const transition = (
  <Transition.Sequence>
    <Transition.Out type="fade" durationMs={constants.ANIMATION_DURATION} />
    <Transition.Change
      interpolation="easeInOut"
      durationMs={constants.ANIMATION_DURATION}
    />
    <Transition.In type="fade" durationMs={constants.ANIMATION_DURATION} />
  </Transition.Sequence>
);

type ViewState = {
  showIndeterminateStateLoading: boolean;
  showLogin: boolean;
  showFeed: boolean;
  showBlurredView: boolean;
};

const HomeScreen: ScreenComponent = () => {
  const {
    setup,
    loginState,
    setCredentials,
    setLoginState,
    removeCredentials
  } = SessionContainer.useContainer();

  const { videoStreamState } = VideoSreamContainer.useContainer();

  const strings = LocalizableContainer.useContainer();

  // boot and resumes

  useEffect(() => {
    setup();
  }, []);

  // UI flow logic state

  const [viewState, setViewState] = useState<ViewState>({
    showIndeterminateStateLoading: true,
    showLogin: false,
    showFeed: false,
    showBlurredView: false
  });

  const transitioningViewRef = useRef<TransitioningView>(null);

  useEffect(() => {
    const loggedIn =
      loginState === LoginState.IN ||
      loginState === LoginState.ERROR_BUT_LOGGED_IN ||
      loginState === LoginState.NEEDS_LOGOUT;

    const showLogin = loginState !== LoginState.UNKKNOWN && !loggedIn;
    const showFeed = loggedIn;
    const showIndeterminateStateLoading = loginState === LoginState.UNKKNOWN;
    const showBlurredLoading = loginState === LoginState.FETCHING;

    if (loginState === LoginState.WRONG_CREDENTIALS) {
      makeToast(strings.ERROR_WRONG_CREDENTIALS, true);
    } else if (loginState === LoginState.ERROR) {
      makeToast(strings.ERROR_STANDARD, true);
    } else if (loginState === LoginState.NEEDS_LOGOUT) {
      Alert.alert(
        strings.LOGOUT,
        strings.LOGOUT_CONFIRM,
        [
          {
            text: strings.NO,
            onPress: () => setLoginState(LoginState.IN),
            style: "cancel"
          },
          { text: strings.YES, onPress: () => removeCredentials() }
        ],
        { cancelable: false }
      );
    }

    transitioningViewRef &&
      transitioningViewRef.current &&
      transitioningViewRef.current.animateNextTransition();

    setViewState({
      showLogin,
      showFeed,
      showIndeterminateStateLoading,
      showBlurredView: showBlurredLoading
    });
  }, [loginState]);

  useEffect(() => {
    setViewState({
      ...viewState,
      showBlurredView: videoStreamState === VideoStreamState.FETCHING
    });

    if (videoStreamState === VideoStreamState.ERROR_UNAUTHORIZED) {
      makeToast(strings.ERROR_UNAUTHORIZED, true);
    } else if (videoStreamState === VideoStreamState.ERROR) {
      makeToast(strings.ERROR_STANDARD, true);
    }
  }, [videoStreamState]);

  // handlers

  const onSubmit = (username: string, password: string) => {
    clearToast();
    setCredentials(username, password);
  };

  return (
    <View style={styles.rootContainer}>
      <Transitioning.View
        ref={transitioningViewRef}
        style={styles.contentContainer}
        transition={transition}
      >
        {viewState.showIndeterminateStateLoading && <WhiteActivityIndicator />}
        {viewState.showLogin && <LoginView onSubmit={onSubmit} />}
        {viewState.showFeed && <FeedView style={styles.feed} />}
      </Transitioning.View>
      <BlurredLoadingView
        style={styles.blurredLoading}
        isVisible={viewState.showBlurredView}
      />
    </View>
  );
};

type ComponentStyle = {
  rootContainer: ViewStyle;
  contentContainer: ViewStyle;
  feed: ViewStyle;
  blurredLoading: ViewStyle;
};

const styles = StyleSheet.create<ComponentStyle>({
  rootContainer: {
    width: "100%",
    height: "100%",
    backgroundColor: colors.PASSION
  },
  contentContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  feed: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
  },
  blurredLoading: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%"
  }
});

export default HomeScreen;
