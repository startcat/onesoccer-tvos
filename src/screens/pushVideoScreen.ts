import {
  VideoMeta,
  pushNativeVideoStreamPlayer
} from "@internal/nativeBindings/pushNativeVideoStreamPlayer";

import { VideoStream } from "@internal/containers/VideoStreamContainer";

const pushVideoScreen = (videoStream: VideoStream) => {
  const meta: VideoMeta = {
    manifestUri: videoStream.manifest,
    userEmail: videoStream.sessionUUID,
    contentTitle: videoStream.altId,
    contentIsLive: videoStream.live
  };
  pushNativeVideoStreamPlayer(meta);
};
export default pushVideoScreen;
