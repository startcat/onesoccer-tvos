import React from "react";
import { ViewProps } from "react-native";

export type ScreenProps = ViewProps & {};
export type ScreenComponent<M = {}> = React.FunctionComponent<ScreenProps & M>;
