const constants = {
  SPACING: 20,
  ANIMATION_DURATION: 612,
  BUTTON_BORDER_RADIUS: 6,
  BUTTON_SIZE: { width: 500, height: 60 },
  SAFE_AREA_LEFT_PADDING: 90
};

export default constants;
