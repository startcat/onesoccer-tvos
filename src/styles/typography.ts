import { TextStyle } from "react-native";

// https://developer.apple.com/design/human-interface-guidelines/tvos/visual-design/typography

const CIRCULAR_BOOK_TYPO = "CircularTT-Book";
const CIRCULAR_BOLD_TYPO = "CircularTT-Black";
const BRAND1_TYPO = "ULer5xYQ0XH2IalLjXKHN6H28AvNbDQlUzr-eBe2.app";
const BRAND2_TYPO = "EQXL_xAdea5eh4dd08XDNwI831zhWm5Wcg6RS7P8.app";

export const BODY_SIZE = 25;

export type Typography = {
  circular: TextStyle;
  circularBold: TextStyle;
  brand1: TextStyle;
  brand2: TextStyle;
};

const typography: Typography = {
  circular: {
    fontFamily: CIRCULAR_BOOK_TYPO,
    fontSize: BODY_SIZE
  },
  circularBold: {
    fontFamily: CIRCULAR_BOLD_TYPO,
    fontSize: BODY_SIZE
  },
  brand1: {
    fontFamily: BRAND1_TYPO,
    fontSize: BODY_SIZE
  },
  brand2: {
    fontFamily: BRAND2_TYPO,
    fontSize: BODY_SIZE
  }
};

export default typography;
