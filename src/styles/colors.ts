const colors = {
  WHITE: "white",
  NEW_BLACK: "#333333",
  LIGHT_GREY: "#F2F3F4",
  ERROR_RED: "red",
  PASSION: "#F40059",
  FOCUS_OPACITY0: "#33333336",
  FOCUS_OPACITY1: "#00000099",
  FOCUS_HIGHLIGHT0: "#ffffff20",
  NIGHT: "#001C75",
  BACKGROUND_ALT: "2B2B2B"
};

export default colors;
