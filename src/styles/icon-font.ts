import { createIconSetFromFontello } from "react-native-vector-icons";

// https://medium.com/@kelleyannerose/react-native-custom-icon-font-with-no-sad-red-screen-72b8d09a0e7b

const Icon = createIconSetFromFontello(
    {
        "name": "onesoccer_logos",
        "css_prefix_text": "icon-",
        "css_use_suffix": false,
        "hinting": true,
        "units_per_em": 1000,
        "ascent": 850,
        "glyphs": [
          {
            "uid": "da018990e8fd7604758b4d01123007d4",
            "css": "24h",
            "code": 59396,
            "src": "custom_icons",
            "selected": true,
            "svg": {
              "path": "M620.6 605.8H588.3V648.9H478.1V602.5H334.4V518.2L483.3 353H588.4V521.6H620.6V605.8ZM478.1 521.6V479.6L439.1 521.6H478.1ZM210.6 352C202.9 351.4 194.7 351.1 185.9 351.1 183.3 351.1 180.8 351.2 178.3 351.3L57.8 405.4C54.6 411.3 52 417.6 50.3 424.1L210.6 352ZM162.6 476.4L153.7 480.4H162.6V476.4ZM251.8 358.6C243.3 356.3 234.6 354.6 225.8 353.6L48.4 433.2C48.3 433.6 48.3 433.9 48.2 434.3 47.6 438.6 47.3 444.3 47.1 450.4L251.8 358.6ZM101.2 480.3H147.9L162.6 473.7C162.6 467.4 162.6 458.8 162.7 452.6L101.2 480.3ZM318.5 458.4C318.9 451.8 319.3 436.2 318.9 431.3 318.4 423.4 316.8 415.7 314.1 408.3L188.1 464.9C181.4 506.7 124.6 539.9 44.9 568.3V648.9H327.4V562H217.1C256.1 553.1 315.1 508.9 318.5 458.4ZM162.8 448.6C162.8 447.6 162.9 446.8 162.9 446.2 163.3 438.4 167.4 433.2 175.9 433.2 181 433.2 184.3 434.8 186.3 438L301.2 386.4C296.8 381.4 291.8 377.1 286.4 373.5L48.6 480.3H92.1L162.8 448.6ZM188.4 462.1L313.3 406.1C310.9 400.1 307.6 394.5 303.6 389.4L187.7 441.4C188.1 443 188.4 444.6 188.5 446.2 188.9 451 188.9 455.9 188.5 460.7 188.5 461.1 188.4 461.7 188.4 462.1ZM281.3 370.3C274.8 366.6 268.1 363.6 261 361.3L46.9 457.5C46.8 464 46.8 470.4 46.8 475.7L281.3 370.3ZM67.9 391.1L152.4 353.1C111.7 358.1 84.5 372.3 67.9 391.1ZM838.9 559.1V580.1L955.1 527.8V506.9ZM955.1 551.4V531.9L838.9 584.1V603.6ZM755.4 596.6L639.2 648.8V648.9H685.7L755.4 617.6ZM755.4 641.2V621.6L694.8 648.9H738.3ZM955.1 581.9L838.9 634.2V648.9H843.4L955.1 598.6ZM955.1 645.9V632L917.7 648.9H948.5ZM755.4 593.9V550.1H838.9V556.4L955.1 504.3V355.6H838.9V456.6H755.4V355.6H639.2V646.2ZM955.1 622.3V607L861.9 648.9H895.9ZM755.4 646.7L750.5 648.9H755.4ZM955.1 556.9L838.9 609.1V627.3L955.1 575.1Z",
              "width": 1000
            },
            "search": [
              "onesoccer_24h_clean"
            ]
          },
          {
            "uid": "2cfb3f2b46b34a1790aec0aa846297b6",
            "css": "menu",
            "code": 59392,
            "src": "entypo"
          },
          {
            "uid": "bc50457410acf467b8b5721240768742",
            "css": "facebook",
            "code": 62220,
            "src": "entypo"
          },
          {
            "uid": "d090355c31f497b61d676416c1fd39fb",
            "css": "twitter",
            "code": 62217,
            "src": "entypo"
          },
          {
            "uid": "0fbfb1cd7a847c88ea4c141d0e1bbdb5",
            "css": "instagram",
            "code": 62253,
            "src": "entypo"
          },
          {
            "uid": "592717bd601645d61517d2a584d04127",
            "css": "left-mini",
            "code": 59393,
            "src": "entypo"
          },
          {
            "uid": "37f6cfbb4062ed0d01b351ec35c334ff",
            "css": "right-mini",
            "code": 59394,
            "src": "entypo"
          },
          {
            "uid": "884cfc3e6e2d456dd2a2ca0dbb9e6337",
            "css": "left",
            "code": 59395,
            "src": "entypo"
          },
          {
            "uid": "004882ab2d5c418c5b2060e80596279b",
            "css": "right",
            "code": 59398,
            "src": "entypo"
          },
          {
            "uid": "7222571caa5c15f83dcfd447c58d68d9",
            "css": "search",
            "code": 59399,
            "src": "entypo"
          },
          {
            "uid": "47a1f80457068fbeab69fdb83d7d0817",
            "css": "youtube",
            "code": 61802,
            "src": "fontawesome"
          },
          {
            "uid": "d10920db2e79c997c5e783279291970c",
            "css": "dots",
            "code": 59400,
            "src": "entypo"
          },
          {
            "uid": "8a8d3a4e6681570bd656a49f413e601b",
            "css": "explore",
            "code": 59401,
            "src": "custom_icons",
            "selected": true,
            "svg": {
              "path": "M20 20H280V280H20ZM20 370H280V630H20ZM20 720H280V980H20ZM370 20H630V280H370ZM370 720H630V980H370ZM720 20H980V280H720ZM720 370H980V630H720ZM720 720H980V980H720ZM370 370H630V630H370Z",
              "width": 1000
            },
            "search": [
              "explore_icon_clean"
            ]
          },
          {
            "uid": "dad37df54ae14b1a5baf5dd3a790b2e9",
            "css": "home",
            "code": 59402,
            "src": "custom_icons",
            "selected": true,
            "svg": {
              "path": "M916.5 293.1L603 6.7C593.3-2.2 578.4-2.2 568.6 6.6L93 437.4C93 437.4 92.9 437.4 92.9 437.5L8.6 513.8C-2 523.4-2.9 540 6.5 550.8 11.6 556.6 18.7 559.6 25.8 559.6 31.9 559.6 38.1 557.4 43 552.9L84.3 515.5V973.7C84.3 988.2 95.9 1000 110.2 1000H455.1C469.3 1000 480.9 988.2 480.9 973.7V668.9H685.8V973.8C685.8 988.2 697.3 1000 711.6 1000H1056.5C1070.8 1000 1082.3 988.3 1082.3 973.8V515.1L1123.6 552.9C1134.2 562.6 1150.6 561.7 1160.1 550.9 1169.6 540.1 1168.7 523.5 1158.1 513.9L916.5 293.1ZM1030.7 468V947.5H737.4V642.7C737.4 628.2 725.9 616.4 711.6 616.4H455.1C440.8 616.4 429.3 628.2 429.3 642.7V947.5H136V468.7L585.7 61.5 888.8 338.3 1030.7 468Z",
              "width": 1167
            },
            "search": [
              "home_icon_clean"
            ]
          },
          {
            "uid": "a0a63aa857fbcf81e3a289b047618923",
            "css": "profile",
            "code": 59403,
            "src": "custom_icons",
            "selected": true,
            "svg": {
              "path": "M612.3 262.8C612.3 141.7 513.4 43.5 391.3 43.5 269.2 43.5 170.3 141.7 170.3 262.8 170.3 384 269.2 482.2 391.3 482.2 513.4 482.2 612.3 384 612.3 262.8ZM215.1 262.8C215.1 166.3 294 88 391.3 88 488.6 88 567.5 166.3 567.5 262.8 567.5 359.4 488.6 437.6 391.3 437.6 344.6 437.6 299.8 419.2 266.7 386.4 233.7 353.7 215.1 309.2 215.1 262.8ZM734.6 841.8C734.6 653.7 580.9 501.1 391.3 501.1 201.7 501.1 48 653.7 48 841.8V867.4C48 916.6 88.2 956.5 137.7 956.5H644.9C694.5 956.5 734.6 916.6 734.6 867.4V841.8ZM689.8 867.4C690.1 879.3 685.4 890.9 677 899.3 668.5 907.7 656.9 912.3 644.9 912H137.7C125.7 912.3 114.1 907.7 105.6 899.3 97.2 890.9 92.5 879.3 92.8 867.4V841.8C92.8 736 149.7 638.2 242.1 585.3 334.4 532.4 448.2 532.4 540.5 585.3 632.9 638.2 689.8 736 689.8 841.8V867.4Z",
              "width": 783
            },
            "search": [
              "profile_icon_clean"
            ]
          },
          {
            "uid": "25fc99a30fecc4021fdcae5fff5ba9ac",
            "css": "eye",
            "code": 59405,
            "src": "entypo"
          },
          {
            "uid": "2d76f2c8d13be40694ecadfea7fb83c3",
            "css": "logout",
            "code": 59406,
            "src": "entypo"
          },
          {
            "uid": "cb13afd4722a849d48056540bb74c47e",
            "css": "play",
            "code": 59407,
            "src": "entypo"
          },
          {
            "uid": "48f2e18872fe74d4579a4c8a1527fc1e",
            "css": "stop",
            "code": 59408,
            "src": "entypo"
          },
          {
            "uid": "d8d378d0ce413f231dfa37592e39c227",
            "css": "pause",
            "code": 59409,
            "src": "entypo"
          },
          {
            "uid": "0afbb00323696f49e9c47f4d9fe6390b",
            "css": "next",
            "code": 59410,
            "src": "entypo"
          },
          {
            "uid": "ceefe7653a4f6edaacce9e7f196cec5a",
            "css": "previous",
            "code": 59411,
            "src": "entypo"
          },
          {
            "uid": "85806fd8ab907f45d34f976354a0df23",
            "css": "fast-backward",
            "code": 59413,
            "src": "entypo"
          },
          {
            "uid": "18f7c393e3532e40edd45607c9d99988",
            "css": "fast-forward",
            "code": 59412,
            "src": "entypo"
          },
          {
            "uid": "413717f5ea649e8d8b93938be277264a",
            "css": "calendar",
            "code": 59415,
            "src": "entypo"
          },
          {
            "uid": "0fb088e4495b53456e205ad1832bedc8",
            "css": "logo",
            "code": 59397,
            "src": "custom_icons",
            "selected": true,
            "svg": {
              "path": "M810.7 807.5V826.1H806.1V807.5H798.9V803.2H817.5V807.5H810.7ZM842.5 825.7V811.1L836.1 825.7H832.5L826.4 811.4V825.7H821.4V802.9H827.5L834.3 818.6 841.1 802.9H846.8V825.7H842.5ZM313.6 592.1C309.6 584.3 306.4 576.4 303.6 568.2L44.6 686.4H173.2C182.5 702.9 193.2 718.6 204.6 733.2L367.1 658.9C344.6 640.7 326.4 618.2 313.6 592.1ZM316.8 405.7L10 545.7H126.8C128.6 560.4 131.1 574.6 134.6 588.9L292.9 516.8C290.4 478.2 298.6 439.6 316.8 405.7L316.8 405.7ZM781.8 234.6C563.9 16.8 185.7 136.1 131.4 437.5 131.4 437.5 384.6 320.7 425.7 303.2 452.5 292.1 481.1 286.4 510 286.1 700 286.1 796.4 513.6 661.1 648.9 642.9 667.5 621.1 682.5 597.1 693.2 591.8 695.4 586.4 697.5 581.1 698.9 528.2 716.4 470.4 713.6 419.3 691.1L128.9 823.9 309.6 824.3C475.4 920.4 686.8 881.1 806.8 731.4 860.4 665 889.6 582.1 889.6 496.8 890 398.6 851.1 304.3 781.8 234.6L781.8 234.6Z",
              "width": 1000
            },
            "search": [
              "thumb_logo_clean"
            ]
          }
        ]
      },
  "onesoccer_logos",
  "onesoccer_logos.ttf"
);

export default Icon;
