import { doFetch } from "@internal/network/services/commons";

type SettingsResponse = {
  apiBaseUrl: string;
  epg: string;
  channel: {
    selectedStream: string;
    name: string;
  };
};

export default function getSettings() {
  return doFetch<SettingsResponse>(async (agent, plugins) => {
    const res = (await agent.get(`/api/v1/settings`)).body;
    plugins.auth.configureBaseUrl = req => {
      req.url = res.apiBaseUrl + req.url;
    };
    return res;
  });
}
