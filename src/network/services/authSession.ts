import { doFetch, RequestType } from "@internal/network/services/commons";

type AuthSessionResponse = {
  success: string;
};

export default function authSession(uuid: string) {
  return doFetch<AuthSessionResponse>(async agent => {
    const auth: AuthSessionResponse = (await agent.get(`/auth/session/${uuid}`))
      .body;
    return auth;
  }, RequestType.Auth);
}
