import {
  doFetch,
  configureRetry,
  RetryCallback
} from "@internal/network/services/commons";
import { FeedItem } from "@internal/network/services/getFeed";

export type VideoInfo = { live: boolean; uid: string; selectedStream: string };

export default function getVideoInfo(
  video: FeedItem,
  retryCallback: RetryCallback
) {
  return doFetch<VideoInfo>(async agent => {
    const info = (await configureRetry(
      agent.get(`/api/v1/videos/${video.id}`),
      retryCallback
    )).body;
    return info as VideoInfo;
  });
}
