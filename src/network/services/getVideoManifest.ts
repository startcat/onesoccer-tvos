import {
  doFetch,
  RequestType,
  RetryCallback,
  configureRetry
} from "@internal/network/services/commons";
import { VideoInfo } from "@internal/network/services/getVideoInfo";

export type VideoManifest = { manifest: string };

export default function getVideoManifest(
  videoInfo: VideoInfo,
  sessionUUID: string,
  retryCallback: RetryCallback
) {
  return doFetch<VideoManifest>(async agent => {
    const endPoint = videoInfo.live
      ? `/media/hls/${videoInfo.selectedStream}/${sessionUUID}`
      : `/media/hls/vod/${videoInfo.uid}/${sessionUUID}`;
    const hls: VideoManifest = (await configureRetry(
      agent.get(endPoint),
      retryCallback
    )).body;
    return hls;
  }, RequestType.Auth);
}
