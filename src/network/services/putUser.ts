import { doFetch } from "@internal/network/services/commons";

type PutAppUserResponse = {
  id: string;
  uuid: string;
  apiKey: string;
};

export default function putUser(email: string, uuid: string, token: string) {
  return doFetch<PutAppUserResponse>(async agent => {
    return (await agent.put(`/api/v1/users/${email}`).send({ uuid, token }))
      .body;
  });
}
