import request from "superagent";

import { API_ENDPOINT } from "react-native-dotenv";
import logger from "@internal/libs/logger";

type AgentPlugin = {
  [key: string]: (req: request.SuperAgentRequest) => void;
};

type ProtocolPlugins = {
  service: AgentPlugin;
  auth: AgentPlugin;
};

const plugins: ProtocolPlugins = {
  service: {},
  auth: {}
};

// agents

const PRODUCTION_ENDPOINT = "https://app.onesoccer.ca";

const agent = request.agent();
agent.use(req => {
  req.url = PRODUCTION_ENDPOINT + req.url;
  Object.values(plugins.service).forEach(plugin => plugin(req));
});

const authAgent = request.agent();
authAgent.use(req =>
  Object.values(plugins.auth).forEach(plugin => plugin(req))
);

// helper w/ error handling the right way

type ServiceOkResponse = {
  error: false;
};

type ServiceErrorResponse = {
  error: true;
  status: number;
};

export enum RequestType {
  Auth = 0,
  Service = 612
}

export const doFetch = async <T>(
  fn: (agent: request.SuperAgentStatic, plugins: ProtocolPlugins) => Promise<T>,
  type: RequestType = RequestType.Service
): Promise<ServiceErrorResponse | (T & ServiceOkResponse)> => {
  try {
    const fnAgent = type === RequestType.Auth ? authAgent : agent;
    const result: T = await fn(fnAgent, plugins);
    return { error: false, ...result };
  } catch (err) {
    if (err.response) {
      const response = err.response;
      logger.error(
        `http error: url=${response.req.url}, status=${
          response.status
        }, text="${response.text}"`
      );
      return { error: true, status: response.status };
    } else {
      logger.error(`non http error: ${err}`);
      return { error: true, status: 0 };
    }
  }
};

export type RetryCallback = () => Promise<boolean>;

export const configureRetry = (
  req: request.SuperAgentRequest,
  callback: RetryCallback
): request.SuperAgentRequest => {
  return req.retry(2, async (_err, res) => {
    if (res.status !== 401) {
      return false;
    }
    return await callback();
  });
};
