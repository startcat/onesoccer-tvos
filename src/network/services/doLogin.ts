import { doFetch, RequestType } from "@internal/network/services/commons";

type LoginResponse = {
  uuid: string;
  token: string;
};

export default function doLogin(
  email: string,
  password: string,
  storedAuth?: LoginResponse
) {
  return doFetch<LoginResponse>(async (agent, plugins) => {
    const auth: LoginResponse = storedAuth
      ? storedAuth
      : (await agent.post("/auth/login").send({ email, password })).body
          .success;

    plugins.auth.configureToken = req => {
      req.set("x-sessionid", auth.token);
    };

    return auth;
  }, RequestType.Auth);
}
