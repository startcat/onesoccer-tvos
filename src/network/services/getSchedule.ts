import request from "superagent";

export type ScheduleResponseItem = {
  error: false;
  id: number;
  live: number;
  title: string;
  date: string;
  dateEnd: string;
};

export type ScheduleResponse =
  | { error: true }
  | {
      error: false;
      data: Array<ScheduleResponseItem>;
    };

export default async function getSchedule(
  url: string
): Promise<ScheduleResponse> {
  try {
    return { error: false, data: (await request.get(url)).body };
  } catch (err) {
    return { error: true };
  }
}
