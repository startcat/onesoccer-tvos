import {
  doFetch,
  RetryCallback,
  configureRetry
} from "@internal/network/services/commons";

export type FeedItem = {
  altId: string;
  date: string;
  header: string;
  id: string;
  title: string;
  live: boolean;
  images: {
    landscape: string;
    thumb: string;
  };
};

type FeedResponse = {
  feed: Array<FeedItem>;
};

export default function getFeed(
  limit: number,
  skip: number,
  retryCallback: RetryCallback
) {
  return doFetch<FeedResponse>(async agent => {
    const feed = (await configureRetry(
      agent
        .get("/api/v1/videos")
        .query(`include24h=true&limit=${limit}&skip=${skip}`),
      retryCallback
    )).body;
    return { feed };
  });
}
