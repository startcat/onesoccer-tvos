import React from "react";

import { AppRegistry } from "react-native";

import HomeScreen from "@internal/screens/home/HomeScreen";
import LocalizableContainer from "@internal/containers/LocalizableContainer";
import SessionContainer from "@internal/containers/SessionContainer";
import FeedContainer from "@internal/containers/FeedContainer";
import VideoSreamContainer from "@internal/containers/VideoStreamContainer";
import ApplicationStateContainer from "@internal/containers/ApplicationStateContainer";
import ScheduleScreen from "@internal/screens/schedule/ScheduleScreen";
import ScheduleContainer from "@internal/containers/ScheduleContainer";

// home

const HomeScreenComponent = () => (
  <LocalizableContainer.Provider>
    <SessionContainer.Provider>
      <FeedContainer.Provider>
        <VideoSreamContainer.Provider>
          <ApplicationStateContainer.Provider>
            <HomeScreen />
          </ApplicationStateContainer.Provider>
        </VideoSreamContainer.Provider>
      </FeedContainer.Provider>
    </SessionContainer.Provider>
  </LocalizableContainer.Provider>
);

AppRegistry.registerComponent("HomeScreen", () => HomeScreenComponent);

// schedule

const ScheduleScreenComponent = () => (
  <LocalizableContainer.Provider>
    <ScheduleContainer.Provider>
      <ScheduleScreen />
    </ScheduleContainer.Provider>
  </LocalizableContainer.Provider>
);

AppRegistry.registerComponent("ScheduleScreen", () => ScheduleScreenComponent);
