//
//  ParallaxView.h
//  OneSoccer
//
//  Created by yellow on 07/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <React/RCTViewManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParallaxViewManager : RCTViewManager
@end

@interface ParallaxImageViewManager : RCTViewManager
@end

@interface FocusableButtonViewManager : RCTViewManager
@end

NS_ASSUME_NONNULL_END
