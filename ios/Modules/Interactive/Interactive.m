//
//  ParallaxView.m
//  OneSoccer
//
//  Created by yellow on 07/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "Interactive.h"

#import <OneSoccer_tvOS-Swift.h>

#pragma mark - ParallaxView

@implementation ParallaxViewManager

RCT_EXPORT_MODULE();

-(UIView*)view
{
  RNParallaxView* view = [[RNParallaxView alloc] init];
  return view;
}

RCT_CUSTOM_VIEW_PROPERTY(cornerRadius, CGFloat, RNParallaxView)
{
  [view updateCornerRadius:[RCTConvert CGFloat:json]];
}

RCT_EXPORT_VIEW_PROPERTY(onPress, RCTBubblingEventBlock)

@end

#pragma mark - ParallaxImageView

@implementation ParallaxImageViewManager

RCT_EXPORT_MODULE();

-(UIView*)view
{
  RNParallaxImageView* view = RNParallaxImageView.new;
  return view;
}

RCT_CUSTOM_VIEW_PROPERTY(imageUrl, NSString*, RNParallaxImageView)
{
  [view updateImageUrl:[RCTConvert NSString:json]];
}

RCT_CUSTOM_VIEW_PROPERTY(cornerRadius, CGFloat, RNParallaxImageView)
{
  [view updateCornerRadius:[RCTConvert CGFloat:json]];
}

RCT_EXPORT_VIEW_PROPERTY(onPress, RCTBubblingEventBlock)

@end

#pragma mark - FocusableButtonView

@interface FocusableButtonView : UIView

@property (nonatomic, copy) RCTBubblingEventBlock onPress;
@property (nonatomic, strong) UIColor* focusedColor;
@property (nonatomic, strong) UITapGestureRecognizer* tapGestureRecognizer;

@end

@implementation FocusableButtonView

-(instancetype)init
{
  self = [super init];
  if (self) {
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    [self addGestureRecognizer:self.tapGestureRecognizer];
  }
  return self;
}

-(void)onTap:(UITapGestureRecognizer*)recognizer
{
  if (_onPress) {
    _onPress(@{});
  }
}

-(BOOL)canBecomeFocused
{
  return YES;
}

-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
  if (!self.focusedColor) {
    return;
  }
  
  if (context.nextFocusedView == self) {
    [coordinator addCoordinatedAnimations:^{
      [UIView animateWithDuration:UIView.inheritedAnimationDuration animations:^{
        self.backgroundColor = self.focusedColor;
      } completion:NULL];
    } completion:NULL];
  }
  else {
    [coordinator addCoordinatedAnimations:^{
      [UIView animateWithDuration:UIView.inheritedAnimationDuration animations:^{
        self.backgroundColor = UIColor.clearColor;
      } completion:NULL];
    } completion:NULL];
  }
}

@end

@implementation FocusableButtonViewManager

RCT_EXPORT_MODULE();

-(UIView*)view
{
  FocusableButtonView* view = FocusableButtonView.new;
  return view;
}

RCT_CUSTOM_VIEW_PROPERTY(focusedColor, UIColor, FocusableButtonView)
{
  view.focusedColor = [RCTConvert UIColor:json];
}

RCT_EXPORT_VIEW_PROPERTY(onPress, RCTBubblingEventBlock)

@end
