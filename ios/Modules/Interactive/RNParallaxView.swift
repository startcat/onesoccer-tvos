//
//  RNParallaxView.swift
//  OneSoccer
//
//  Created by yellow on 07/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import ParallaxView
import SDWebImage

@objc
class RNParallaxView: ParallaxView {
  @objc var onPress: RCTDirectEventBlock?

  let glowView = UIView()
  
  required init?(coder aDecoder: NSCoder) {
    fatalError()
  }
  
  init() {
    super.init(frame: .zero)
    
    addSubview(glowView)
    glowView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    
    parallaxEffectOptions.glowContainerView = glowView
    
    parallaxViewActions.setupFocusedState = { (view) -> Void in
      //self.parallaxEffectOptions.glowAlpha = Double(0.8)
      //self.parallaxEffectOptions.shadowPanDeviation = Double(200)
      //self.parallaxEffectOptions.parallaxMotionEffect.viewingAngleX = CGFloat(Double.pi/4/90)
      //self.parallaxEffectOptions.parallaxMotionEffect.viewingAngleY = CGFloat(Double.pi/4/90)
      //self.parallaxEffectOptions.parallaxMotionEffect.panValue = CGFloat(5)
      
      view.transform = CGAffineTransform(scaleX: 1.078, y: 1.078)
      
      view.layer.shadowColor = UIColor.black.cgColor
      view.layer.shadowOffset = CGSize(width: 10, height: 10)
      view.layer.shadowOpacity = 0.4
      view.layer.shadowRadius = 30
    }
    
    parallaxViewActions.setupUnfocusedState = { (view) -> Void in
      view.transform = CGAffineTransform.identity
      
      view.layer.shadowOffset = CGSize(width: 0, height: 0)
      view.layer.shadowOpacity = 0
      view.layer.shadowRadius = 0
    }
    
    parallaxViewActions.beforeResignFocusAnimation = { $0.layer.zPosition = -20 }
    parallaxViewActions.beforeBecomeFocusedAnimation = { $0.layer.zPosition = 100 }

    let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(RNParallaxView.tapped(_:)))
    addGestureRecognizer(tapRecognizer)
  }
  
  @objc func updateCornerRadius(_ cr: CGFloat) {
    cornerRadius = cr
  }
  
  override func addSubview(_ view: UIView) {
    super.addSubview(view)
    
    if (!view.isEqual(glowView)) {
      bringSubviewToFront(glowView)
    }
  }

  @objc
  func tapped(_ sender: UITapGestureRecognizer) {
    if onPress != nil {
      onPress!(nil)
    }
  }
}

@objc
class RNParallaxImageView: RNParallaxView {
  
  let imageView = UIImageView()
  
  required init?(coder aDecoder: NSCoder) {
    fatalError()
  }
  
  override init() {
    super.init()

    addSubview(imageView)
    imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
  }

  @objc func updateImageUrl(_ imageUrl: String) {
    imageView.sd_setImage(with: URL(string: imageUrl))
  }
  
  override func updateCornerRadius(_ cr: CGFloat) {
    cornerRadius = cr
    imageView.layer.cornerRadius = cr
    imageView.layer.masksToBounds = true
  }
}
