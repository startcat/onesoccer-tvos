//
//  LocalizableManager.m
//  OneSoccer
//
//  Created by yellow on 22/05/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "LocalizableManager.h"

@implementation LocalizableManager

RCT_EXPORT_MODULE();

+(BOOL)requiresMainQueueSetup
{
  return YES;
}

-(NSDictionary*)constantsToExport
{
  NSString* stringsPath = [[NSBundle mainBundle] pathForResource:@"Localizable" ofType:@"strings"];
  NSDictionary* dict = [NSDictionary dictionaryWithContentsOfFile:stringsPath];
  return dict ? dict : @{};
}


@end
