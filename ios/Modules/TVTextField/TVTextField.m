//
//  TVTextField.m
//  OneSoccer-tvOS
//
//  Created by yellow on 25/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "TVTextField.h"

#import <JSONModel/JSONModel.h>

#pragma mark - json font > object

@interface TVTextFieldViewFont : JSONModel

@property (nonatomic, strong) NSString* fontFamily;
@property (nonatomic, assign) NSInteger fontSize;

@end

@implementation TVTextFieldViewFont
@end

#pragma mark - view / private

@interface TVTextFieldView ()

@property (nonatomic, copy) RCTBubblingEventBlock onChangeText;
@property (nonatomic, strong) UIFont* fontValue;
@property (nonatomic, strong) NSString* placeholderValue;
@property (nonatomic, strong) NSString* textValue;

@end

#pragma mark - view / implementation

@implementation TVTextFieldView

-(void)setFontValue:(UIFont*)fontValue
{
  _fontValue = fontValue;
  
  if (_placeholderValue) {
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_placeholderValue
                                                                 attributes:@{NSFontAttributeName:_fontValue}];
  }
  self.font = _fontValue;
}

-(void)setPlaceholderValue:(NSString*)placeholderValue
{
  _placeholderValue = placeholderValue;
  
  if (_fontValue) {
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_placeholderValue
                                                                 attributes:@{NSFontAttributeName:_fontValue}];
  }
}

-(void)setTextValue:(NSString *)textValue
{
  _textValue = textValue;
  
  self.text = _textValue;
}

#pragma mark UITextFieldDelegate

-(void)textFieldDidEndEditing:(UITextField *)textField
{
  if (_onChangeText) {
    _onChangeText(@{@"text": textField.text});
  }
}

@end

#pragma mark - view manager

@implementation TVTextFieldViewManager

RCT_EXPORT_MODULE();

-(UIView*)view
{
  TVTextFieldView* view = [TVTextFieldView new];
  view.backgroundColor = UIColor.whiteColor;
  view.delegate = view;
  return view;
}

RCT_CUSTOM_VIEW_PROPERTY(font, NSDictionary, TVTextFieldView)
{
  NSError* err;
  TVTextFieldViewFont* fontSpec = [[TVTextFieldViewFont alloc] initWithDictionary:[RCTConvert NSDictionary:json] error:&err];
  NSAssert(err == nil, @"TVTextFieldView / wrong font");
  
  view.fontValue = [UIFont fontWithName:fontSpec.fontFamily size:fontSpec.fontSize];
}

RCT_CUSTOM_VIEW_PROPERTY(secureTextEntry, BOOL, TVTextFieldView)
{
  view.secureTextEntry = [RCTConvert BOOL:json];
}

RCT_EXPORT_VIEW_PROPERTY(textValue, NSString*)
RCT_EXPORT_VIEW_PROPERTY(placeholderValue, NSString*)
RCT_EXPORT_VIEW_PROPERTY(onChangeText, RCTBubblingEventBlock)

@end
