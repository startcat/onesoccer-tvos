//
//  TVTextField.h
//  OneSoccer-tvOS
//
//  Created by yellow on 25/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <React/RCTViewManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TVTextFieldView : UITextField<UITextFieldDelegate>
@end

@interface TVTextFieldViewManager : RCTViewManager
@end


NS_ASSUME_NONNULL_END
