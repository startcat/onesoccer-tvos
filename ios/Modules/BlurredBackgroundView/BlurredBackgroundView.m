//
//  BlurredBackgroundViewManager.m
//  OneSoccer
//
//  Created by yellow on 07/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "BlurredBackgroundView.h"

#import <OneSoccer_tvOS-Swift.h>

@implementation BlurredBackgroundViewManager

RCT_EXPORT_MODULE();

-(UIView*)view
{
  BlurView* view = [[BlurView alloc] init];
  return view;
}

RCT_CUSTOM_VIEW_PROPERTY(animationValue, CGFloat, BlurView)
{
  [view updateBlurEffect:[RCTConvert CGFloat:json]];
}

@end
