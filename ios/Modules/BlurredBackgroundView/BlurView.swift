import Foundation
import ParallaxView

@objc
class BlurView: UIView {
  private let effectView: UIVisualEffectView
  private var animator: UIViewPropertyAnimator?
  
  required init?(coder aDecoder: NSCoder) {
    fatalError()
  }
  
  init() {
    self.effectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
    
    super.init(frame: .zero)
    
    // https://github.com/tzahola/UIViewPropertyAnimator-UIVisualEffectView/blob/master/Interactive%20Blur%20Sample/ViewController.m
    let blur = self.effectView.effect
    self.effectView.effect = nil
    self.animator = UIViewPropertyAnimator(duration: 1, curve: .linear) {
      self.effectView.effect = blur
    }
    self.animator?.stopAnimation(true)

    backgroundColor = UIColor.clear
    insertSubview(effectView, at: 0)
  }
  
  override func addSubview(_ view: UIView) {
    effectView.contentView.addSubview(view)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    effectView.frame = bounds
  }
  
  override func willMove(toWindow newWindow: UIWindow?) {
    if (newWindow == nil) {
      animator?.stopAnimation(true)
    }
    super.willMove(toWindow: newWindow)
  }
  
  @objc func updateBlurEffect(_ value: CGFloat) {
    guard let a = animator else {
      return
    }
    
    a.fractionComplete = value
  }
}
