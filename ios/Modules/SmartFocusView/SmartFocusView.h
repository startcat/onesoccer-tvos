//
//  SmartFocusView.h
//  OneSoccer
//
//  Created by yellow on 07/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <React/RCTViewManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface SmartFocusView : UIView

@property (nonatomic, weak) UIView* preferredSmartFocusedView;

@end

@interface SmartFocusViewManager : RCTViewManager
@end

NS_ASSUME_NONNULL_END
