//
//  SmartFocusView.m
//  OneSoccer
//
//  Created by yellow on 07/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "SmartFocusView.h"

#import "AppDelegate.h"
#import "UIView+IAY.h"

@interface SmartFocusView ()

@property (nonatomic, strong) NSString* areaType;
@property (nonatomic, readonly) SmartFocusNavigationController* controller;

@property (nonatomic, strong) UIFocusGuide* focusGuide;
@property (nonatomic, assign) BOOL needsFocusGuide;
@property (nonatomic, assign) BOOL claimsFirstFocus;
@property (nonatomic, assign) BOOL doNotStorePreferredSmartFocus;

@property (nonatomic, copy) RCTBubblingEventBlock onFocusDidChange;

@property (nonatomic, assign) BOOL isMounted;
@property (nonatomic, assign) BOOL isUnmounting;
@property (nonatomic, assign) BOOL isFocused;

@property (nonatomic, strong) UITapGestureRecognizer* menuButtonGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer* swipeRightGestureRecognizer;

@end

@implementation SmartFocusView

#pragma mark - focus

-(void)didUpdateFocusInContext:(UIFocusUpdateContext*)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
  SmartFocusNavigationController* nc = self.controller;
  BOOL wasFocused = _isFocused;
  BOOL isFocused = NO;
  BOOL isMenu = _areaType && [_areaType isEqualToString:@"menu"];
  BOOL isMain = _areaType && [_areaType isEqualToString:@"main"];

  BOOL isSubview = [context.nextFocusedView isDescendantOfView:self];
  if (!isSubview && (isMenu || isMain)) {
    // if the focus change to a view not in menu / main, ignore everything
    if (isMenu && nc.mainAreaSmartFocusView) {
      if (![context.nextFocusedView isDescendantOfView:nc.mainAreaSmartFocusView]) {
        return;
      }
    }
    else if (isMain && nc.menuAreaSmartFocusView) {
      if (![context.nextFocusedView isDescendantOfView:nc.menuAreaSmartFocusView]) {
        return;
      }
    }
  }

  if (_areaType && isSubview) {
    SmartFocusView* closest = (SmartFocusView*)[context.nextFocusedView iay_getParentWhichMatchBlock:^BOOL(UIView * _Nonnull view) {
      return [view isKindOfClass:SmartFocusView.class];
    }];
    if (closest && !closest.doNotStorePreferredSmartFocus) {
      _preferredSmartFocusedView = context.nextFocusedView;
    }
  }

  if (isSubview) {
    isFocused = YES;
  }

  _isFocused = isFocused;
  
  if (isFocused != wasFocused) {
    if (isMenu && isFocused) {
      nc.mainAreaSmartFocusView.userInteractionEnabled = NO;
      [nc doUpdateFocus];
    }
    
    if (_onFocusDidChange) {
      _onFocusDidChange(@{@"isFocused": @(isFocused)});
    }
  }
}

#pragma mark - lifecycle

// https://bradbambara.wordpress.com/2015/01/18/object-life-cycle-uiview/

-(void)didMoveToSuperview
{
  if (!self.superview && _isMounted && _isUnmounting) {
    _isMounted = _isUnmounting = NO;
    [self doUnmount];
  }
  [super didMoveToSuperview];
}

-(void)didMoveToWindow
{
  [super didMoveToWindow];

  if (self.window && self.superview && !_isMounted) {
    _isMounted = YES;
    [self doMount];
  }
  else if (!self.window && self.superview && _isMounted) {
    _isUnmounting = YES;
  }
}

-(void)doMount
{
  if (!_focusGuide && _needsFocusGuide) {
    _focusGuide = UIFocusGuide.new;
    [self addLayoutGuide:_focusGuide];

    // lc

    NSLayoutConstraint* wc = [_focusGuide.widthAnchor constraintEqualToAnchor:self.widthAnchor];
    NSLayoutConstraint* hc = [_focusGuide.heightAnchor constraintEqualToAnchor:self.heightAnchor];
    NSLayoutConstraint* tc = [_focusGuide.topAnchor constraintEqualToAnchor:self.topAnchor];
    NSLayoutConstraint* lc = [_focusGuide.leftAnchor constraintEqualToAnchor:self.leftAnchor];
    [NSLayoutConstraint activateConstraints:@[wc, hc, tc, lc]];
  }

  SmartFocusNavigationController* nc = self.controller;

  if ([_areaType isEqualToString:@"menu"]) {
    nc.menuAreaSmartFocusView = self;
    
    _swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                             action:@selector(swipeRight:)];
    _swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:_swipeRightGestureRecognizer];

    _menuButtonGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuButton:)];
    _menuButtonGestureRecognizer.allowedPressTypes = @[[NSNumber numberWithInteger:UIPressTypeMenu]];
    [self addGestureRecognizer:_menuButtonGestureRecognizer];
  }
  else if ([_areaType isEqualToString:@"main"]) {
    nc.mainAreaSmartFocusView = self;
  }
  
  if (_claimsFirstFocus) {
    // this also is going to trigger nc focus update
    [nc claimFirstFocusForView:self];
  }
}

-(void)doUnmount
{
  // clean
  
  if (_focusGuide) {
    [self removeLayoutGuide:_focusGuide];
  }
  if (_swipeRightGestureRecognizer) {
    [self removeGestureRecognizer:_swipeRightGestureRecognizer];
  }
  if (_menuButtonGestureRecognizer) {
    [self removeGestureRecognizer:_menuButtonGestureRecognizer];
  }
  
  if ([_areaType isEqualToString:@"menu"]) {
    SmartFocusNavigationController* nc = self.controller;
    nc.menuAreaSmartFocusView = nil;
  }
  else if ([_areaType isEqualToString:@"main"]) {
    SmartFocusNavigationController* nc = self.controller;
    nc.mainAreaSmartFocusView = nil;
  }
}

#pragma mark - gestures

-(void)restoreMainAreaInteraction
{
  SmartFocusNavigationController* nc = self.controller;
  if (!nc.mainAreaSmartFocusView.userInteractionEnabled) {
    nc.mainAreaSmartFocusView.userInteractionEnabled = YES;
    [nc doUpdateFocus];
  }
}

-(void)swipeRight:(UISwipeGestureRecognizer*)recognizer
{
  [self restoreMainAreaInteraction];
}

-(void)menuButton:(UITapGestureRecognizer*)recognizer
{
  [self restoreMainAreaInteraction];
}

#pragma mark - helpers

-(SmartFocusNavigationController*)controller
{
  AppDelegate* delegate = (AppDelegate*)UIApplication.sharedApplication.delegate;
  return delegate.navigationController;
}

@end

#pragma mark - RN view manager

@implementation SmartFocusViewManager

RCT_EXPORT_MODULE();

-(UIView*)view
{
  SmartFocusView* view = [SmartFocusView new];
  return view;
}

RCT_EXPORT_METHOD(updateFocus)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate* delegate = (AppDelegate*)UIApplication.sharedApplication.delegate;
    [delegate.navigationController doUpdateFocus];
  });
}

RCT_EXPORT_VIEW_PROPERTY(onFocusDidChange, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(areaType, NSString*)
RCT_EXPORT_VIEW_PROPERTY(needsFocusGuide, BOOL)
RCT_EXPORT_VIEW_PROPERTY(claimsFirstFocus, BOOL)
RCT_EXPORT_VIEW_PROPERTY(doNotStorePreferredSmartFocus, BOOL)

@end
