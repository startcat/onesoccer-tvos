//
//  SmartFocusNavigationController.m
//  OneSoccer
//
//  Created by yellow on 14/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "SmartFocusNavigationController.h"

#import "UIView+IAY.h"
#import "TVTextField.h"

@interface SmartFocusNavigationController ()

@end

@implementation SmartFocusNavigationController

/*
-(void)didUpdateFocusInContext:(UIFocusUpdateContext *)context withAnimationCoordinator:(UIFocusAnimationCoordinator *)coordinator
{
  UIView* nextView = context.nextFocusedView;
  if ([nextView isKindOfClass:[TVTextFieldView class]]) {
    TVTextFieldView* tf = (TVTextFieldView*)nextView;
    if (tf.text.length > 0) {
      [self doUpdateFocus];
    }
  }
}*/

-(NSArray<id<UIFocusEnvironment>> *)preferredFocusEnvironments
{
  NSPredicate* predicate = [NSPredicate predicateWithBlock:^BOOL(UIView*  _Nullable view,
                                                                 NSDictionary<NSString *,id> * _Nullable bindings) {
    return view.canBecomeFocused;;
  }];

  if (_mainAreaSmartFocusView && _mainAreaSmartFocusView.userInteractionEnabled) {
    UIView* pfe = _mainAreaSmartFocusView.preferredSmartFocusedView;
    if (pfe) {
      return @[pfe];
    }
    pfe = [_mainAreaSmartFocusView iay_findFirstSubviewRecursivelyWithPredicate:predicate];
    if (pfe) {
      return @[pfe];
    }
  }
  else if (self.menuAreaSmartFocusView) {
    UIView* pfe = [_menuAreaSmartFocusView iay_findFirstSubviewRecursivelyWithPredicate:predicate];
    if (pfe) {
      return @[pfe];
    }
  }
  
  /*
  else {
    // login form?
    NSPredicate* predicate2 = [NSPredicate predicateWithBlock:^BOOL(UIView*  _Nullable view,
                                                                   NSDictionary<NSString *,id> * _Nullable bindings) {
      if ([view isKindOfClass:[TVTextFieldView class]]) {
        TVTextFieldView* tf = (TVTextFieldView*)view;
        if (tf.text.length > 0) {
          return NO;
        }
      }
      return view.canBecomeFocused;
    }];
    NSArray* pfe = [self.visibleViewController.view iay_getAllSubviewsRecursivelyWithPredicate:predicate2];
    if (pfe && pfe.count > 0) {
      return pfe;
    }
  }*/
  return [super preferredFocusEnvironments];
}

-(void)claimFirstFocusForView:(SmartFocusView*)view
{
  if (_menuAreaSmartFocusView && [view isDescendantOfView:_menuAreaSmartFocusView]) {
    _menuAreaSmartFocusView.preferredSmartFocusedView = view;
  }
  else if (_mainAreaSmartFocusView && [view isDescendantOfView:_mainAreaSmartFocusView]) {
    _mainAreaSmartFocusView.preferredSmartFocusedView = view;
  }

  [self doUpdateFocus];
}

-(void)doUpdateFocus
{
  dispatch_async(dispatch_get_main_queue(), ^(){
    [self setNeedsFocusUpdate];
    [self updateFocusIfNeeded];
  });
}

@end
