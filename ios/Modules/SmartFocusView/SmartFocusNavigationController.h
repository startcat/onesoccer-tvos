//
//  SmartFocusNavigationController.h
//  OneSoccer
//
//  Created by yellow on 14/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SmartFocusView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SmartFocusNavigationController : UINavigationController

@property (nonatomic, weak) SmartFocusView* menuAreaSmartFocusView;
@property (nonatomic, weak) SmartFocusView* mainAreaSmartFocusView;

-(void)claimFirstFocusForView:(SmartFocusView*)view;
-(void)doUpdateFocus;

@end

NS_ASSUME_NONNULL_END
