//
//  NativeScreensViewController.m
//  OneSoccer-tvOS
//
//  Created by yellow on 15/07/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "NativeScreensViewController.h"

#import "AppDelegate.h"

#pragma mark - custom root view as a workaround to solve this: https://github.com/facebook/react-native/issues/18930

@interface RCTTVRemoteHandler : NSObject

@property (nonatomic, copy, readonly) NSDictionary *tvRemoteGestureRecognizers;

@end

@interface IAYRootView : RCTRootView

@property (nonatomic, strong) RCTTVRemoteHandler *tvRemoteHandler;

@end

@implementation IAYRootView

- (instancetype)initWithBridge:(RCTBridge *)bridge
                    moduleName:(NSString *)moduleName
             initialProperties:(NSDictionary *)initialProperties
{
  self = [super initWithBridge:bridge moduleName:moduleName initialProperties:initialProperties];
  if (self) {
    for (NSString *key in [self.tvRemoteHandler.tvRemoteGestureRecognizers allKeys]) {
      [self removeGestureRecognizer:self.tvRemoteHandler.tvRemoteGestureRecognizers[key]];
    }
  }
  return self;
}
@end

#pragma mark - private

@interface NativeScreensViewController ()

@property (nonatomic, strong) NSString* moduleName;

@end

#pragma mark - implementation

@implementation NativeScreensViewController

-(instancetype)initWithModuleName:(NSString*)moduleName
{
  self = [super init];
  if (self) {
    _moduleName = moduleName;
  }
  return self;
}

-(void)loadView
{
  AppDelegate* delegate = (AppDelegate*)UIApplication.sharedApplication.delegate;
  self.view = [[IAYRootView alloc] initWithBridge:delegate.bridge
                                       moduleName:_moduleName
                                initialProperties:nil];
  self.view.backgroundColor = _bgColor ? _bgColor : UIColor.clearColor;
}

@end
