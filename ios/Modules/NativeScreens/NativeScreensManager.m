//
//  NativeScreensModule.m
//  OneSoccer-tvOS
//
//  Created by yellow on 15/07/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "NativeScreensManager.h"

#import "NativeScreensViewController.h"

#import "AppDelegate.h"

@implementation NativeScreensManager

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(pushScreen:(NSString*)moduleName)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    NativeScreensViewController* vc = [[NativeScreensViewController alloc] initWithModuleName:moduleName];
    AppDelegate* delegate = (AppDelegate*)UIApplication.sharedApplication.delegate;
    [delegate.navigationController pushViewController:vc animated:YES];
  });
}

@end
