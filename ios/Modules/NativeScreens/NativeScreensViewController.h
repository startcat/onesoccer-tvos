//
//  NativeScreensViewController.h
//  OneSoccer-tvOS
//
//  Created by yellow on 15/07/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <React/RCTRootView.h>
#import <React/RCTBridge.h>

NS_ASSUME_NONNULL_BEGIN

@interface NativeScreensViewController : UIViewController

@property (nonatomic, strong) UIColor *bgColor;

-(instancetype)initWithModuleName:(NSString*)moduleName;

@end

NS_ASSUME_NONNULL_END
