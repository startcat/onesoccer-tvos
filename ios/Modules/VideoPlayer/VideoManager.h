//
//  VideoManager.h
//  OneSoccer
//
//  Created by yellow on 05/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <React/RCTBridgeModule.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoManager : NSObject <RCTBridgeModule>
@end

NS_ASSUME_NONNULL_END
