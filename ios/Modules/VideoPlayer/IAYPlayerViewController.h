//
//  IAYPlayerViewController.h
//  OneSoccer
//
//  Created by yellow on 05/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <AVKit/AVKit.h>

#import "VideoMeta.h"

NS_ASSUME_NONNULL_BEGIN

@interface IAYPlayerViewController : AVPlayerViewController

-(instancetype)initWithMeta:(VideoMeta*)meta;

@end

NS_ASSUME_NONNULL_END
