//
//  IAYPlayerViewController.m
//  OneSoccer
//
//  Created by yellow on 05/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

// https://github.com/NPAW/avplayer-adapter-ios/blob/master/AVPlayerAdaptertvOSExample/AVPlayerAdaptertvOSExample/PlayerViewController.m

#import "IAYPlayerViewController.h"

#import <YouboraAVPlayerAdapter/YouboraAVPlayerAdapter.h>

@interface IAYPlayerViewController ()

@property (nonatomic, strong) VideoMeta* meta;

@property (nonatomic, strong) YBPlugin* youboraPlugin;
@property (nonatomic, strong) YBAVPlayerAdapter* youboraAdapter;

@end

@implementation IAYPlayerViewController

-(instancetype)initWithMeta:(VideoMeta*)meta
{
  self = [super init];
  if (self) {
    self.player = [AVPlayer playerWithURL:[NSURL URLWithString:meta.manifestUri]];
    self.meta = meta;
    self.showsPlaybackControls = YES;
  }
  return self;
}

-(void)viewDidLoad
{
  [super viewDidLoad];

  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];

  [YBLog setDebugLevel:YBLogLevelSilent];

  YBOptions* youboraOptions = [YBOptions new];
  youboraOptions.offline = NO;
  youboraOptions.accountCode = @"golt";
  youboraOptions.contentTransactionCode = @"canada";
  youboraOptions.userEmail = self.meta.userEmail;
  youboraOptions.contentTitle = self.meta.contentTitle;
  youboraOptions.contentIsLive = @(self.meta.contentIsLive);
  self.youboraPlugin = [[YBPlugin alloc] initWithOptions:youboraOptions];

  self.youboraAdapter = [[YBAVPlayerAdapter alloc] initWithPlayer:self.player];
  [self.youboraPlugin setAdapter:self.youboraAdapter];
  [self.youboraPlugin fireInit];

  [self doStart];
}

-(void)doStart
{
  [self.youboraPlugin enable];
  [self.player play];
}

-(void)doStop
{
  [self.youboraPlugin disable];
  [self.player pause];
}

-(void)viewWillDisappear:(BOOL)animated
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [self doStop];
  
  [super viewWillDisappear:animated];
}

-(void)appDidBecomeActive:(NSNotification*)notification
{
  [self doStart];
}

-(void)appWillResignActive:(NSNotification*)notification
{
  [self doStop];
}

@end
