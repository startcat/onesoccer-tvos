//
//  VideoMeta.h
//  OneSoccer
//
//  Created by yellow on 05/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <JSONModel/JSONModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoMeta : JSONModel

@property (nonatomic, strong) NSString* manifestUri;
@property (nonatomic, strong) NSString* userEmail;
@property (nonatomic, strong) NSString* contentTitle;
@property (nonatomic, assign) BOOL contentIsLive;

@end

NS_ASSUME_NONNULL_END
