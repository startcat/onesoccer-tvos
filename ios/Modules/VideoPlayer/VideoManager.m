//
//  VideoManager.m
//  OneSoccer
//
//  Created by yellow on 05/06/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "VideoManager.h"

#import <AVKit/AVKit.h>

#import "IAYPlayerViewController.h"
#import "AppDelegate.h"

@implementation VideoManager

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(pushVideoStream:(NSDictionary*)metaDictionary)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate* delegate = (AppDelegate*)UIApplication.sharedApplication.delegate;
    
    IAYPlayerViewController* playerViewController = [[IAYPlayerViewController alloc] initWithMeta:[[VideoMeta alloc] initWithDictionary:metaDictionary error:nil]];
    
    [delegate.navigationController pushViewController:playerViewController animated:YES];
  });
}

@end
