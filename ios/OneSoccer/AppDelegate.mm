/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"

#import <AVKit/AVKit.h>
#import <JavaScriptCore/JavaScriptCore.h>

#import <React/RCTRootView.h>
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>

#import "IAYPlayerViewController.h"
#import "NativeScreensViewController.h"

@interface AppDelegate()

@end

@implementation AppDelegate

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  // https://developer.apple.com/documentation/avfoundation/media_assets_playback_and_editing/creating_a_basic_video_player_ios_and_tvos
  AVAudioSession* audioSession = [AVAudioSession sharedInstance];
  NSError* err;
  [audioSession setCategory:AVAudioSessionCategoryPlayback
                       mode:AVAudioSessionModeMoviePlayback
                    options:NULL
                      error:&err];
  
  _bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  
  UIColor* brandColor = [UIColor colorWithRed:244/255 green:0 blue:89/255 alpha:1];

  // vc
  NativeScreensViewController* rootViewController = [[NativeScreensViewController alloc] initWithModuleName:@"HomeScreen"];
  rootViewController.bgColor = brandColor;

  //nc
  self.navigationController = [[SmartFocusNavigationController alloc] initWithRootViewController:rootViewController];
  self.navigationController.navigationBarHidden = YES;

  // window
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  self.window.backgroundColor = brandColor;
  self.window.rootViewController = self.navigationController;
  [self.window makeKeyAndVisible];
  
  return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

@end
